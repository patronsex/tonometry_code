const mysql = require('promise-mysql');
const config = require('../config/production');
const Hemera = require('nats-hemera');
const HemeraJoi = require('hemera-joi');
const nats = require('nats').connect(process.env.NATS_URL || config.nats.url);

const hemera = new Hemera(nats, {
    logLevel: 'info',
    childLogger: true,
    tag: 'hemera-insert'
});

async function start() {
    const con = await mysql.createConnection({
        host: config.database.host,
        port: config.database.port,
        user: config.database.user,
        password: config.database.password,
        database: config.database.database
    });
    //подаждать запуск подключения к базе данных
    hemera.use(HemeraJoi);
    //подаждать пока микросервис себя создаст
    await hemera.ready();
    //создание экземпляра валитора входящих данных
    const Joi = hemera.joi;

    hemera.add({
            topic: 'add',
            cmd: 'database',
            doctor_ip: Joi.string().required(),
            doctor_id: Joi.number().required(),
            patient_id: Joi.number().required(),
            pressure: Joi.string().required(),
            quality: Joi.string().required(),
            diagnosis: Joi.string().required(),
            therapy: Joi.string().required(),
            recommendations: Joi.string().required()
        },
        async (req) => {
            try {
                await con.query("INSERT INTO history (doctor_ip, doctor_id, patient_id, date, updated_at, pressure, quality, diagnosis, therapy, recommendations) VALUES (?,?,?,now(),now(),?,?,?,?,?)",[req.doctor_ip, req.doctor_id, req.patient_id, req.pressure, req.quality, req.diagnosis, req.therapy, req.recommendations]);
                return {success:1}
            }
            catch (e) {
                return {success:0};
            }
        });

    hemera.add({
            topic: 'delete',
            cmd: 'database',
            session_id: Joi.number().required(),
        },
        async (req) => {
            try {
                await con.query("DELETE FROM history WHERE session_id=?",[req.session_id]);
                return {success:1}
            }
            catch (e) {
                return {success:0};
            }
        });

    hemera.add({
            topic: 'get',
            cmd: 'database',
            doctor_id: Joi.number().required(),
        },
        async (req) => {
            try {
                let result = await con.query("SELECT * FROM patients AS p LEFT OUTER JOIN (SELECT * FROM history GROUP BY session_id) AS h ON h.patient = p.patient_id AND p.last_reception = h.date AND p.last_session_id = h.session_id WHERE p.doctor_id = '?'",[req.doctor_id]);
                return result;
            }
            catch (e) {
                return {success:0};
            }
        });

    hemera.add({
            topic: 'patient_history',
            cmd: 'database',
            patient_id: Joi.number().required(),
            page: Joi.number().required()
        },
        async (req) => {
        let page = await req.page;
        if (page == 1){

        }
        let limit = [(Number(page)*5-5),5];
            try {
                let result = await con.query("SELECT * FROM `history` WHERE `patient` = '?' ORDER BY `history`.`date` DESC LIMIT ?,?",[req.patient_id, limit[0], limit[1]]); //SELECT * FROM `history` WHERE `patient` = '?'
                return result;
            }
            catch (e) {
                return {success:0};
            }
        });

    hemera.add({
            topic: 'edit',
            cmd: 'database',
            doctor_ip: Joi.string().required(),
            doctor_id: Joi.number().required(),
            patient_id: Joi.number().required(),
            session_id: Joi.number().required(),
            pressure: Joi.string().required(),
            quality: Joi.string().required(),
            diagnosis: Joi.string().required(),
            therapy: Joi.string().required(),
            recommendations: Joi.string().required()
        },
        async (req) => {
            try {
                await con.query("UPDATE history SET doctor_ip=?, doctor_id=?, patient_id=?, updated_at=now(), pressure=?, quality=?, diagnosis=?, therapy=?, recommendations=? WHERE `session_id` = ? ",[req.doctor_ip, req.doctor_id, req.patient_id, req.pressure, req.quality, req.diagnosis, req.therapy, req.recommendations, req.session_id]);
                return {success:1}
            }
            catch (e) {
                return {success:e};
            }
        });

}

module.exports = start;