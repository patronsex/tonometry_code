const axios = require("axios");

console.log(Date.now());

(async () => {
    const res = await axios.delete("http://localhost:3000/api/v1/history/delete", {
        data: { session_id: 96 }
    })
        .then((res)=>{
            console.log(res.data)})
        .catch((error)=>{
            console.log(error);
        });

})();