const params = {
  doc_id: 123,
  pat_id: 123
};

const keys = Object.keys(params);
const query_params = "UPDATE history SET " + keys.reduce((acc, cur, index) => {
    if(keys.length - 1 !== index) return acc + cur + " = ?, ";
    else return acc + cur + " = ?";
},"");

console.log(query_params);