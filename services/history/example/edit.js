const axios = require("axios");

//await con.query("INSERT INTO history (doctor_id, date, updated_at, pressure, diagnosis, therapy, recommendations) VALUES (" + req.doctor_id +
//             ", now(), now(), "+ req.pressure +","+ req.diagnosis +","+ req.therapy +","+ req.recommendations +")");
console.log(Date.now());

(async () => {
    const res = await axios.post("http://localhost:3000/api/v1/history/edit", {
        doctor_ip:'127.0.2.1',
        doctor_id:10000,
        patient_id:120,
        session_id:97,
        pressure:"{\"maklakovLeft\": 2546, \"maklakovRight\": 5675}",
        quality:"{\"Left\": 979, \"Right\": 95678}",
        diagnosis:"{\"glaucom\": 96546540}",
        therapy:"{\"therapy\": 56546450}",
        recommendations:"{\"read\": 15776546456455}"
    })
        .then((res)=>{
            console.log(res.data)})
        .catch((error)=>{
            console.log(error);
        });

})();