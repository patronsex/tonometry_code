const mysql = require('promise-mysql');

const addFilters = ({sql, filters, mysql}) => {
    return new Promise(async resolve => {
        if (filters) {
            sql += " WHERE";
            filters.map(({type, operator, field}) => {
                sql += " ";
                if (type) {
                    sql += type;
                    sql += ` ${field} `;
                } else {
                    sql += `${field} `;
                }
                sql += operator;
                sql += " ?"
            });
            return resolve(await mysql.format(sql, filters.map(item => item.value)));
        } else {
            return resolve(sql);
        }
    });
};


(async () => {
    const res = await addFilters({
        sql: 'SELECT * FROM history',
        mysql: mysql,
        filters: [
            {
                operator: "=",
                field: "doctor_id",
                value: 77
            },
            {
                operator: ">",
                type: "or",
                field: "session_id",
                value: 100
            },
            {
                operator: "=",
                type: "and",
                field: "date",
                value: '2020-01-25 20:06:31'
            }
        ]
    });
    console.log(res);
})();