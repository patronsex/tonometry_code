const axios = require("axios");

//await con.query("INSERT INTO history (doctor_id, date, updated_at, pressure, diagnosis, therapy, recommendations) VALUES (" + req.doctor_id +
//             ", now(), now(), "+ req.pressure +","+ req.diagnosis +","+ req.therapy +","+ req.recommendations +")");
console.log(Date.now());

(async () => {
    const res = await axios.post("http://localhost:3000/api/v1/history/add", {
        doctor_ip:"127.0.0.1",
        doctor_id:77,
        patient_id:88,
        pressure:"{\"maklakovLeft\": 246, \"maklakovRight\": 525}",
        quality:"{\"Left\": 99, \"Right\": 98}",
        diagnosis:"{\"glaucom\": 90}",
        therapy:"{\"therapy\": 50}",
        recommendations:"{\"read\": 15775}"
    })
        .then((res)=>{
            console.log(res.data)})
        .catch((error)=>{
        console.log(error);
    });

})();