import React from 'react';
import './styles/App.sass';
import {createBrowserHistory} from "history";
import {Router, Switch, Route} from "react-router";
import MainPage from "./pages/Main";
import MassesPage from "./pages/Masses";
import UploadPage from "./pages/Upload";
import {observer, Provider} from "mobx-react";
import ResultPage from "./pages/Result";
import PatientInfoPage from "./pages/PatientInfo";
import Login from "./pages/Login";
import HistoryPage from "./pages/History";
import PatientPage from "./pages/PatientPage";
import ElastoCalculator from "./pages/ElastoCalculator";
import Logout from "./pages/Logout";
import ProcedureStore from "./mobx/ProcedureStore";

const history = createBrowserHistory();

@observer
class App extends React.Component {

    constructor(props) {
        super(props);
        //стор который хранит и определяет порядок процедуры
        this.procedureStore = new ProcedureStore();
    }

    render() {

        const {procedureStore} = this;

        return (
            <Provider
                procedureStore={procedureStore}
            >
                <div className="App">
                    <Router history={history}>
                        <Switch>
                            <Route path="/" component={MainPage} exact/>
                            {/* модуль измерения давления */}
                            <Route path="/upload" component={UploadPage} exact/>
                            <Route path="/patient" component={() => <PatientInfoPage/>} exact/>
                            <Route path="/masses" component={() => <MassesPage/>} exact/>
                            <Route path="/result" component={ResultPage} exact/>
                            {/* end модуль измерения давления */}

                            {/* модуль реестра и групп */}
                            <Route path="/history" component={HistoryPage} exact/>
                            <Route path="/patient_page/:patient/" component={PatientPage} exact/>
                            {/* end модуль реестра и групп*/}

                            {/* модуль авторизации - регистрации */}
                            <Route path="/login" component={Login} exact/>
                            <Route path="/logout" component={Logout}/>
                            {/* end модуль авторизации - регистрации */}

                            {/*Эластические свойства фиброзной оболочки глаза*/}
                            <Route path="/calculator" component={ElastoCalculator} exact/>
                            {/*Эластические свойства фиброзной оболочки глаза*/}
                        </Switch>
                    </Router>
                </div>
            </Provider>
        );
    }
}

export default App;
