import {action, observable} from "mobx";

function isNil(value) {
    return value === null || value === undefined;
}

export default class ProcedureStore {
    //деклорация workflow процедуры

    @observable selected_procedure = null;
    @observable current_page = null;

    @action setProcedure(selected_procedure) {
        this.selected_procedure = selected_procedure;
    }

    sequences = {
        "default_procedure": {
            "urls": {
                "/upload": {
                    validate: [],
                    title: 'Загрузить'
                },
                "/masses": {
                    validate: ["boxes", "masses", "ratios", "eyes"],
                    title: 'Массы'
                },
                "/patient": {
                    validate: [],
                    title: 'О пациенте'
                },
                "/result": {
                    validate: [],
                    title: 'Результат'
                }
            },
            "base_url": "/"
        }
    };

    @action setCurrentPage(link) {
        this.current_page = link;
    }

    getUrlsWithTitle() {
        if (this.selected_procedure) {
            const keys = Object.keys(this.sequences[this.selected_procedure].urls);
            let urls = {};
            for (let i = 0; i < keys.length; i++) {
                const url = keys[i];
                const current = this.sequences[this.selected_procedure].urls[url];
                urls[url] = current.title;
                if(url === this.current_page) break;
            }
            return urls;
        } else {
            return null;
        }

    }

    getNext() {
        if (this.selected_procedure) {
            const sequence = this.sequences[this.selected_procedure].urls;
            const links = Object.keys(sequence);
            const current_index = links.indexOf(this.current_page);
            if (current_index === links.length - 1 || current_index === -1) {
                return false;
            } else {
                return links[current_index + 1];
            }
        } else {
            return false;
        }
    }

    validate(url) {
        return new Promise((resolve, reject) => {
            if (this.selected_procedure) {
                const sequence = this.sequences[this.selected_procedure].urls;

                if (sequence.hasOwnProperty(url)) {
                    for (let i = 0; i < sequence[url].validate.length; i++) {
                        const current_field = sequence[url].validate[i];
                        if (isNil(this[current_field])) return reject({
                            success: 0,
                            message: `Поле ${current_field} не может быть пустым`,
                            redirectTo: i >= 1 ? Object.keys(sequence)[i - 1] : this.sequences[this.selected_procedure]["base_url"]
                        });
                    }
                    return resolve({
                        success: 1
                    });
                } else return reject({
                    success: 0,
                    message: "Данный компонент не участвует в процедуре",
                    redirectTo: '/'
                });
            } else {
                return reject({
                    success: 0,
                    message: "Не выбран тип процедуры",
                    redirectTo: '/'
                })
            }
        });
    }


    //данные полученные от нейросети, которая размечает изображение с оттискамм
    //устанавливает компонент Upload
    @observable marked_image = null;
    @observable boxes = null;
    @observable masses = null;
    @observable ratios = null;
    @observable eyes = null;

    @action setMarkupImageResponse({marked_image, boxes, masses, ratios, eyes} = {}) {
        if (marked_image) this.marked_image = marked_image;
        if (boxes) this.boxes = boxes;
        if (masses) this.masses = masses;
        if (ratios) this.ratios = ratios;
        if (eyes) this.eyes = eyes;
    }

    //end


    //данные полученные после рассчетов
    //устанавливает компонент Masses
    @observable p5_right = null;
    @observable p5_left = null;
    @observable p10_right = null;
    @observable p10_left = null;
    @observable p15_right = null;
    @observable p15_left = null;
    @observable p5_0_right = null;
    @observable p5_0_left = null;
    @observable p10_0_right = null;
    @observable p10_0_left = null;
    @observable p15_0_right = null;
    @observable p15_0_left = null;
    @observable p0_right = null;
    @observable p0_left = null;
    @observable ep_left = null;
    @observable ep_right = null;
    @observable b_l = null;
    @observable b_r = null;

    @action setCalculatedData({p5_right, p5_left, p10_right, p10_left, p15_right, p15_left, p5_0_right, p5_0_left, p10_0_right, p10_0_left, p15_0_right, p15_0_left, p0_right, p0_left, ep_left, ep_right, b_l, b_r} = {}) {
        if (p5_right) this.p5_right = p5_right;
        if (p5_left) this.p5_left = p5_left;
        if (p10_right) this.p10_right = p10_right;
        if (p10_left) this.p10_left = p10_left;
        if (p15_right) this.p15_right = p15_right;
        if (p15_left) this.p15_left = p15_left;
        if (p5_0_right) this.p5_0_right = p5_0_right;
        if (p5_0_left) this.p5_0_left = p5_0_left;
        if (p10_0_right) this.p10_0_right = p10_0_right;
        if (p10_0_left) this.p10_0_left = p10_0_left;
        if (p15_0_right) this.p15_0_right = p15_0_right;
        if (p15_0_left) this.p15_0_left = p15_0_left;
        if (p0_right) this.p0_right = p0_right;
        if (p0_left) this.p0_left = p0_left;
        if (ep_left) this.ep_left = ep_left;
        if (ep_right) this.ep_right = ep_right;
        if (b_l) this.b_l = b_l;
        if (b_r) this.b_r = b_r;
    }

    //данные о пациенте
    //устанавливает компонент PreparationsPage
    @observable stage_left = null;
    @observable stage_right = null;
    @observable medication_left = null;
    @observable medication_right = null;
    @observable day = null;
    @observable month = null;
    @observable year = null;


    @action setPatientInfo({stage_left, stage_right, medication_left, medication_right, day, month, year} = {}) {
        if (stage_left) this.stage_left = stage_left;
        if (stage_right) this.stage_right = stage_right;
        if (medication_left) this.medication_left = medication_left;
        if (medication_right) this.medication_right = medication_right;
        if (day) this.day = day;
        if (month) this.month = month;
        if (year) this.year = year;
    }
}
