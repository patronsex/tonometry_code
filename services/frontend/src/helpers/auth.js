import {auth} from "../../../config/production";
import axios from "axios";
import {stringify} from "querystring";

function isNil(value) {
    return value == null
}

export function logout() {
    localStorage.removeItem("access");
    localStorage.removeItem("refresh");
    window.location = `https://ai-tonometry.online/oauth2/logout?client_id=${auth.app_id}`
}

export function setToken(access, refresh) {
    if(access) localStorage.setItem("access", access);
    if(refresh) localStorage.setItem("refresh", access);
}

export function getToken() {
    const access = localStorage.getItem("access");
    const refresh = localStorage.getItem("refresh");
    return {
        access: isNil(access) ? null : access,
        refresh: isNil(refresh) ? null : refresh
    };
}

export function userIsLogged() {
    const {access, refresh} = getToken();
    return !!(access && refresh);
}

function redirectToLoginPage() {
    window.location = `${auth.auth_server}/oauth2/authorize?client_id=${auth.app_id}&response_type=code&redirect_uri=${auth.redirect_uri}&scope=offline_access%20openid`;
}

export function authorize(code) {
    return new Promise(async (resolve, reject) => {

        if (code) {
            const encodedSecret = Buffer.from(`${auth.app_id}:${auth.client_secret}`).toString('base64');

            return await axios.post(`${auth.auth_server}/oauth2/token`, stringify({
                code: code,
                client_id: auth.app_id,
                grant_type: "authorization_code",
                redirect_uri: auth.redirect_uri
            }), {
                headers: {
                    Authorization: `Basic ${encodedSecret}`,
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            }).then(res => {
                const auth = res.data;
                localStorage.setItem("access", auth.access_token);
                localStorage.setItem("refresh", auth.refresh_token);
                return resolve(res.data)
            }).catch(() => reject(null));
        } else {
            redirectToLoginPage();
            return resolve();
        }
    });
}

export function loginIfNotAuthorized() {
    if (!userIsLogged()) redirectToLoginPage();
}
