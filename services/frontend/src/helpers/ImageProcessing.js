class ImageProcessing {
    static Base64toBlob(base64) {
        let tmp = base64.split(',');
        let data = atob(tmp[1]);
        let mime = tmp[0].split(':')[1].split(';')[0];
        let buf = new Uint8Array(data.length);
        for (let i = 0; i < data.length; i++) {
            buf[i] = data.charCodeAt(i);
        }

        return new Blob([buf], {type: mime});
    }

    static blobToDataURL(blob, callback) {
        const a = new FileReader();
        a.onload = function (e) {
            callback(e.target.result);
        };
        a.readAsDataURL(blob);
    }

    static addBorderToImage(image) {
        return new Promise(resolve => {
            const canvas = document.createElement('canvas');
            const img = new Image();
            img.src = image;
            img.onload = function () {
                canvas.width  = img.width + 160;
                canvas.height = img.height + 160;
                const ctx = canvas.getContext('2d');
                ctx.beginPath();
                ctx.lineWidth = 40;
                ctx.strokeStyle = "black";
                ctx.rect(20, 20, img.width + 120, img.height + 120);
                ctx.stroke();
                ctx.beginPath();
                ctx.lineWidth = 40;
                ctx.strokeStyle = "white";
                ctx.rect(60, 60, img.width + 40, img.height + 40);
                ctx.stroke();
                ctx.beginPath();
                ctx.drawImage(img, canvas.width / 2 - img.width / 2, canvas.height / 2 - img.height / 2);
                ctx.stroke();
                const base64 = canvas.toDataURL('image/jpeg');
                resolve(base64);
            }
        });
    }
}

export default ImageProcessing;
