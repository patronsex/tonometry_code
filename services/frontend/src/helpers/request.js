import {getToken, setToken, logout} from "./auth.js";
import {auth} from "../../../config/production";
import axios from "axios";
import {stringify} from "querystring";

async function refreshToken() {

    const {_, refresh} = getToken();

    const encodedSecret = Buffer.from(`${auth.app_id}:${auth.client_secret}`).toString('base64');

    return new Promise(async (resolve, reject) => {
        await axios.post(`${auth.auth_server}/oauth2/token`, stringify({
            client_id: auth.app_id,
            grant_type: 'refresh_token',
            refresh_token: refresh
        }), {
            headers: {
                Authorization: `Basic ${encodedSecret}`,
                "Content-Type": "application/x-www-form-urlencoded"
            }
        }).then(res => {
            const {access_token} = res.data;
            if (access_token) {
                setToken(access_token);
                return resolve(access_token);
            } else {
                return reject();
            }
        }).catch(err => {
            return reject(err.response);
        })
    });
}

async function request(url, method, params, headers) {

    const {access} = getToken();

    return new Promise(async (resolve, reject) => {
        await axios({
            url: url,
            method: method,
            data: params,
            headers: Object.assign({
                Authorization: `Bearer ${access}`
            }, headers)
        }).then(res => resolve(res)).catch(err => reject(err.response.data));
    });
}

export function authorizedRequest(url = "", method = "GET", params = {}, headers) {
    return new Promise((resolve, reject) => {
        request(url, method, params, headers)
            .then(res => resolve(res.data))
            .catch(async err => {
                if (err.hasOwnProperty("error_reason") && err.error_reason === "access_token_expired") {
                    await refreshToken()
                        .then(async () => {
                            await request(url, method, params, headers).then(res => resolve(res.data))
                        })
                        .catch(() => {
                            logout();
                        });
                }
            })
    });
}