import React, {useState} from "react";
import {NavLink} from "react-router-dom";
import Loader from "../components/Loader";
import HeaderMobile from "../components/Mobile/HeaderMobile";
import TnPageHeader from "../components/PageHeader";
import {Icon} from "antd";
import Footer from "../components/Footer";
import classNames from "classnames";
import {userIsLogged} from "../helpers/auth.js";

export default (props) => {

    const [menu, setMenu] = useState(window.innerWidth >= 1000);
    const [help, setHelp] = useState(window.innerWidth >= 1000);
    const [width, setWidth] = useState(window.innerWidth);

    const {content, rightbar, rightbarTitle} = props;

    function resize() {
        setWidth(window.innerWidth);
        if (width <= 1000) {
            setHelp(false);
            setMenu(false);
        } else {
            setHelp(true);
            setMenu(true);
        }
    }

    window.addEventListener("resize", resize);

    const {additionalMenuItems} = props;
    const menuKeys = additionalMenuItems && Object.keys(additionalMenuItems);

    return (
        <div className="main-layout">
            {width <= 420 && <HeaderMobile
                actionMenu={() => {
                    setMenu(!menu);
                    if (width <= 1000) setHelp(false);
                }}
                actionHelp={() => {
                    if (width <= 1000) setMenu(false);
                    setHelp(!help);
                }}/>}
            <div
                className={classNames("main-layout__sidebar", {
                    "sidebar_show": menu,
                    "sidebar_hide": !menu
                })}>
                <div className="main-layout__logo">
                    <h2>AI Tonometry</h2>
                </div>

                <div className="main-layout__menu">

                    <div className="main-layout__menu-item">
                        <NavLink exact={true} to="." activeClassName="menu_item_active">Главная</NavLink>
                    </div>

                    {menuKeys && menuKeys.map((link, key) => {
                        return <div className="main-layout__menu-item" key={key}>
                            <NavLink exact={true} to={link} activeClassName="menu_item_active">{additionalMenuItems[link]}</NavLink>
                        </div>
                    })}

                </div>
            </div>
            <Loader/>
            <div className={classNames("main-layout__content", {
                "without_sidebar": !menu,
                "without_help": !help
            })}>
                {(menu || help) && <div className="overlay" onClick={() => {
                    setMenu(false);
                    setHelp(false);
                }}/>}
                <TnPageHeader
                    logged={userIsLogged()}
                    menuAction={() => {
                        setMenu(!menu);
                        if (width <= 1000) setHelp(false);
                    }}
                    helpAction={() => {
                        if (width <= 1000) setMenu(false);
                        setHelp(!help);
                    }}
                />
                <div className="main-layout__content-wrapper">
                    {content}
                </div>
                <Footer/>
            </div>
            <div
                className={classNames("main-layout__right-panel", {
                    "right-panel_show": help,
                    "right-panel_hide": !help
                })}>
                <div className="right-panel__header">
                    <span className="right-panel__title">{rightbarTitle}</span>
                </div>
                <div className="right-panel__content">
                    {rightbar}
                    <div className="right-panel__footer">
                        <button className="right-panel__close" onClick={() => {
                            setHelp(false);
                        }}>
                            <Icon type="arrow-right"/>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
};