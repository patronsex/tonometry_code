import React from "react";
import {Icon} from "antd";

class MobileSelectLayout extends React.Component {

    state = {
        selected: []
    };

    componentDidMount() {
        const {selected} = this.props;
        if (selected) this.setState({selected});
    }

    render() {

        const {onCancel, onSave, description, data} = this.props;
        const {selected} = this.state;
        return (
            <div className="mobile-select">
                <div>
                    <div className="mobile-select__logo">
                        <h2>AI Tonometry</h2>
                        <Icon type="menu-fold" onClick={() => {
                            if (typeof onCancel === "function") onCancel();
                        }}/>
                    </div>
                    <div className="mobile-select__description">
                        <p>{description}</p>
                    </div>
                </div>
                <div className="mobile-select__content">
                    {data && data.map((item, key) => {
                        if (selected.indexOf(item) + 1)
                            return <div key={key} className="content__item content-item-active"
                                        onClick={() => {
                                            let selectedWithFilter = selected.filter(f_item => f_item !== item);
                                            this.setState({selected: selectedWithFilter});
                                            this.props.onChange(selectedWithFilter);
                                        }}><Icon
                                type="close-circle"/> {item}</div>;

                        else
                            return <div key={key} className="content__item" onClick={() => {
                                if (!selected.indexOf(item) + 1)
                                    selected.push(item);
                                this.setState({selected: selected});
                                this.props.onChange(selected);
                            }}><Icon
                                type="check-circle"/> {item}</div>
                    })}
                </div>
                <div className="mobile-select__footer">
                    <button className="badge badge-white" onClick={() => {
                        if (typeof onCancel === "function") onCancel();
                    }}>Закрыть
                    </button>
                    <button className="badge badge-white" onClick={() => {
                        if (typeof onSave === "function") onSave(selected);
                        if (typeof onCancel === "function") onCancel();
                    }}>Подтвердить
                    </button>
                </div>
            </div>
        );
    }
}

export default MobileSelectLayout;