import React from "react";
import classNames from "classnames";

class Loader extends React.Component {
    render() {
        const {show} = this.props;
        return (
            <div className={classNames("loader", {
                "loader-show": show
            })}>
                <div className="container-loader">
                    <div className="loader-box"/>
                    <div className="loader-box"/>
                    <div className="loader-box"/>
                    <div className="loader-box"/>
                    <div className="loader-box"/>
                </div>
            </div>
        );
    }
}

export default Loader;