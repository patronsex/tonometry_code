import React from "react";
import {Grid, Row, Col} from "react-flexbox-grid";
import {Icon} from "antd";

class HeaderMobile extends React.Component {
    render() {
        const {actionMenu, actionHelp} = this.props;
        return (
            <div className="header_mobile">
                <Grid>
                    <Row>
                        <Col xs={4}>
                            <div className="header_mobile_item" onClick={() => {
                                if (typeof actionMenu === "function") actionMenu();
                            }}>
                                <Icon type="menu"/>
                                <div>Меню</div>
                            </div>
                        </Col>
                        <Col xs={4}>
                            <div className="header_mobile_item">
                                <Icon type="file-search"/>
                                <div>Измерения</div>
                            </div>
                        </Col>
                        <Col xs={4}>
                            <div className="header_mobile_item" onClick={() => {
                                if (typeof actionHelp === "function") actionHelp();
                            }}>
                                <Icon type="question-circle"/>
                                <div>Справка</div>
                            </div>
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

export default HeaderMobile;