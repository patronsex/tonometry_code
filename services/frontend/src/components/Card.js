import React from "react";

class Card extends React.Component {

    render() {

        const {cardTitle, icon, description, actions} = this.props;

        return (
            <div className="card">
                <div className="card_content">
                    <div className="card_header">
                        <div className="card-icon">
                            {icon}
                        </div>

                        <div className="card-title">
                            <h2><span>{cardTitle}</span></h2>
                        </div>
                    </div>

                    <div className="card-description">
                        {description}
                    </div>

                    <div className="card-actions">
                        {actions}
                    </div>
                </div>
            </div>
        );
    }
}

export default Card;