import React from "react";
import {Icon} from "antd";
import {Link} from "react-router-dom";

class TnPageHeader extends React.Component {

    render() {

        const {children, menuAction, helpAction, logged} = this.props;

        return (
            <div className="page-header">
                <div className="page-header__content-wrapper">
                    <div className="header-item" onClick={() => {
                        if (typeof menuAction === "function") menuAction();
                    }}>
                        <Icon type="menu"/>
                        <div>Меню</div>
                    </div>
                    {children}
                    <div className="header-items-group">
                        {logged && <Link to="/logout">
                            <div className="header-item">
                                <Icon type="logout"/>
                                <div>Выход</div>
                            </div>
                        </Link>}
                        {!logged && <Link to="/login">
                            <div className="header-item">
                                <Icon type="login"/>
                                <div>Войти</div>
                            </div>
                        </Link>}
                        <div className="header-item" onClick={() => {
                            if (typeof helpAction === "function") helpAction();
                        }}>
                            <Icon type="question-circle"/>
                            <div>Справка</div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default TnPageHeader;