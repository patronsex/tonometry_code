import {frontend as config} from "../../../config/production";

class PressureCalculation {

    static safe_div(n1, n2) {
        if (!n2) return 0;
        else return n1 / n2;
    }

    static get_p5({values, rations} = {}) {
        try {
            return {
                left_eye: Object.keys(values).map((value, i) => {
                    const mass = parseInt(values[value].split('-')[0]);
                    const ration = rations[i];
                    const eye = parseInt(values[value].split('-')[1]);
                    console.log(values);
                    if (mass === 5 && eye === 0) return 493.36 * Math.pow((config.ration * ration), -2.048);
                }).filter(item => item !== undefined),
                right_eye: Object.keys(values).map((value, i) => {
                    const mass = parseInt(values[value].split('-')[0]);
                    const ration = rations[i];
                    const eye = parseInt(values[value].split('-')[1]);
                    if (mass === 5 && eye === 1) return 493.36 * Math.pow((config.ration * ration), -2.048);
                }).filter(item => item !== undefined)
            }
        }
        catch (e) {
            return null;
        }
    }

    static get_p10({values, rations} = {}) {
        try {
            return {
                left_eye: Object.keys(values).map((value, i) => {
                    const mass = parseInt(values[value].split('-')[0]);
                    const ration = rations[i];
                    const eye = parseInt(values[value].split('-')[1]);
                    if (mass === 10 && eye === 0) return 931.65 * Math.pow((config.ration * ration), -2.048);
                }).filter(item => item !== undefined),
                right_eye: Object.keys(values).map((value, i) => {
                    const mass = parseInt(values[value].split('-')[0]);
                    const ration = rations[i];
                    const eye = parseInt(values[value].split('-')[1]);
                    if (mass === 10 && eye === 1) return 931.65 * Math.pow((config.ration * ration), -2.048);
                }).filter(item => item !== undefined)
            }
        }
        catch (e) {
            return null;
        }
    }

    static get_p15({values, rations} = {}) {
        try {
            return {
                left_eye: Object.keys(values).map((value, i) => {
                    const mass = parseInt(values[value].split('-')[0]);
                    const ration = rations[i];
                    const eye = parseInt(values[value].split('-')[1]);
                    if (mass === 15 && eye === 0) return 1378.8 * Math.pow((config.ration * ration), -2.048);
                }).filter(item => item !== undefined),
                right_eye: Object.keys(values).map((value, i) => {
                    const mass = parseInt(values[value].split('-')[0]);
                    const ration = rations[i];
                    const eye = parseInt(values[value].split('-')[1]);
                    if (mass === 15 && eye === 1) return 1378.8 * Math.pow((config.ration * ration), -2.048);
                }).filter(item => item !== undefined)
            }
        }
        catch (e) {
            return null;
        }
    }

    static get_p0_for_different_mass(p5, p10, p15) {
        const reducer = (accumulator, currentValue) => accumulator + currentValue;
        return {
            p5_0_l: PressureCalculation.safe_div(p5.left_eye.reduce(reducer), p5.left_eye.length),
            p5_0_r: PressureCalculation.safe_div(p5.right_eye.reduce(reducer), p5.right_eye.length),
            p10_0_l: PressureCalculation.safe_div(p10.left_eye.reduce(reducer), p10.left_eye.length),
            p10_0_r: PressureCalculation.safe_div(p10.right_eye.reduce(reducer), p10.right_eye.length),
            p15_0_l: PressureCalculation.safe_div(p15.left_eye.reduce(reducer), p15.left_eye.length),
            p15_0_r: PressureCalculation.safe_div(p15.right_eye.reduce(reducer), p15.right_eye.length)
        }
    }

    static get_elastoplasticity(p15_0_l, p5_0_l, p15_0_r, p5_0_r) {
        return {
            ep_l: p15_0_l - p5_0_l,
            ep_r: p15_0_r - p5_0_r
        }
    }

    static get_approximate_value(p5, p10, p15) {

        const reducer = (accumulator, currentValue) => accumulator + currentValue;

        // calculate coefficient for least squares method for left eye
        const s1_l = p5.left_eye.reduce(reducer) + p10.left_eye.reduce(reducer) + p15.left_eye.reduce(reducer);
        // сумма всех давлений (сумма у на графике)
        const s2_l = 5 * p5.left_eye.length + 10 * p10.left_eye.length + 15 * p15.left_eye.length;
        // сумма всех масс (сумма х на графике)
        const s3_l = 5 * p5.left_eye.reduce(reducer) + 10 * p10.left_eye.reduce(reducer) + 15 * p15.left_eye.reduce(reducer);
        // сумма произведений масс на давление (сумма х*у)
        const s4_l = 5 * 5 * p5.left_eye.length + 10 * 10 * p10.left_eye.length + 15 * 15 * p15.left_eye.length;
        // сумма квадратов масс (сумма квадратов х)
        const n_l = p5.left_eye.length + p10.left_eye.length + p15.left_eye.length;
        // количество всех точек
        const b_l = PressureCalculation.safe_div((n_l * s3_l - (s1_l * s2_l)), (n_l * s4_l - s2_l * s2_l));
        // коэффициент прямой


        // calculate coefficient for least squares method for right eye
        const s1_r = p5.right_eye.reduce(reducer) + p10.right_eye.reduce(reducer) + p15.right_eye.reduce(reducer);
        const s2_r = 5 * p5.right_eye.length + 10 * p10.right_eye.length + 15 * p15.right_eye.length;
        const s3_r = 5 * p5.right_eye.reduce(reducer) + 10 * p10.right_eye.reduce(reducer) + 15 * p15.right_eye.reduce(reducer);
        const s4_r = 5 * 5 * p5.right_eye.length + 10 * 10 * p10.right_eye.length + 15 * 15 * p15.right_eye.length;
        const n_r = p5.right_eye.length + p10.right_eye.length + p15.right_eye.length;
        const b_r = PressureCalculation.safe_div((n_r * s3_r - (s1_r * s2_r)), (n_r * s4_r - s2_r * s2_r));

        // approximate p0 for left eye
        const p0_l = PressureCalculation.safe_div(s1_l, n_l) - b_l * PressureCalculation.safe_div(s2_l, n_l);

        // approximate p0 for right eye
        const p0_r = PressureCalculation.safe_div(s1_r, n_r) - b_r * PressureCalculation.safe_div(s2_r, n_r);

        return {p0_l, p0_r, b_l, b_r};
    }
}

export default PressureCalculation;