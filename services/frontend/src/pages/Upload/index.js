import React from "react";
import Main from "../../layout/Main";
import {Grid, Row, Col} from "react-flexbox-grid";
import {inject, observer} from "mobx-react";
import Cropper from 'react-cropper';
import 'cropperjs/dist/cropper.css';
import {Redirect} from "react-router";
import {Icon} from 'antd';
import ImageProcessing from "../../helpers/ImageProcessing";
import {Upload, message} from 'antd';
import ImagesAPI from "../../api/images";
import Loader from "../../components/Loader";

const {Dragger} = Upload;

@inject("procedureStore")
@observer
class UploadPage extends React.Component {

    constructor(props) {
        super(props);
        this.cropper = React.createRef();
        this.state = {
            redirect: false,
            redirectTo: null,
            rotate: 0,
            imageUrl: null,
            width: window.innerWidth,
            height: window.innerHeight,
            image: null,
            marked_image: null,
            load: false
        };
    }

    updateDimensions = () => {
        this.setState({width: window.innerWidth, height: window.innerHeight});
    };

    componentDidMount() {
        window.addEventListener('resize', this.updateDimensions);
        this.props.procedureStore.setCurrentPage('/upload');
        this.props.procedureStore.validate('/upload').catch(err => {
            this.setState({redirect: true, redirectTo: err.redirectTo});
        });
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateDimensions);
    }

    async _crop() {
        this.setState({load: true});
        const croppedFile = this.cropper.current.getCroppedCanvas().toDataURL('image/jpeg');
        const imageWithBorder = await ImageProcessing.addBorderToImage(croppedFile);
        const response = await ImagesAPI.sendCroppedFile(imageWithBorder).catch(() => null);
        if (response && response.success) {
            const {image, boxes, masses, mask, eyes, ratios} = response.data;
            await this.props.procedureStore.setMarkupImageResponse({
                marked_image: image,
                boxes,
                masses,
                mask,
                eyes,
                ratios
            });
            this.setState({marked_image: image, load: false}, () => {
                const nextPage = this.props.procedureStore.getNext();
                if (nextPage) {
                    this.setState({redirect: true, redirectTo: nextPage});
                }
            });

        } else {
            message.error('Офтальмотонометрические оттиски не найдены!');
            this.setState({marked_image: null, image: null, load: false});
        }
    }

    rotateLeft() {
        this.cropper.current.rotate(90);
    }

    rotateRight() {
        this.cropper.current.rotate(-90);
    }

    beforeUpload(file) {
        const isJpgOrPng = file.type.toLocaleLowerCase() === 'image/jpeg' || file.type.toLocaleLowerCase() === 'image/png';
        if (!isJpgOrPng) {
            message.error('Поддерживаются только фотографии JPG/PNG формата!');
        } else {
            ImageProcessing.blobToDataURL(file, url => {
                this.setState({image: url});
            });
        }

        //чтобы дропзона не делала автоматическую отправку нужно всегда отдавать false
        return false;
    }

    render() {

        const {image, marked_image, width, redirect, redirectTo} = this.state;
        const {procedureStore} = this.props;
        if (redirect) return <Redirect to={redirectTo}/>;

        const uploadProps = {
            name: 'file',
            multiple: false,
            beforeUpload: this.beforeUpload.bind(this),
            showUploadList: false
        };

        const additionalMenuItems = procedureStore.getUrlsWithTitle();

        return (
            <>
                <Loader show={this.state.load}/>
                <Main
                    additionalMenuItems={additionalMenuItems}
                    content={
                        <div className="upload-page">
                            <Grid fluid>
                                <div className="upload-page__from">
                                    <div className="upload-page__upload">
                                        {!image && <div className="instruments">
                                            <Dragger {...uploadProps}>
                                                <p className="ant-upload-drag-icon">
                                                    <Icon type="plus"/>
                                                </p>
                                                <p className="ant-upload-text">Нажмите на + для загрузки фотографии
                                                    или
                                                    перетяните ее из папки</p>
                                                <p className="ant-upload-hint">
                                                    Поддерживаются фотографии JPEG/JPG и PNG формата
                                                </p>
                                            </Dragger>
                                        </div>}
                                        {!marked_image && image &&
                                        <div className="upload-page__cropper">
                                            {width > 420 && <Cropper
                                                ref={this.cropper}
                                                cropBoxMovable={true}
                                                cropBoxResizable={true}
                                                scalable={false}
                                                dragMode="move"
                                                src={image}
                                                style={{height: '400px'}}
                                                initialAspectRatio={16 / 9}
                                                guides={true}
                                            />}

                                            {!(width > 420) && <Cropper
                                                ref={this.cropper}
                                                cropBoxMovable={false}
                                                cropBoxResizable={false}
                                                scalable={false}
                                                dragMode="move"
                                                src={image}
                                                style={{height: '100%'}}
                                                initialAspectRatio={16 / 9}
                                                guides={true}
                                            />}

                                            <div className="upload-page__cropper-actions panel">
                                                <Grid fluid>
                                                    <Row>
                                                        {width > 420 ? <Col xs={4}>
                                                            <div className="panel_items items_left">
                                                                <div className="panel_item" onClick={() => {
                                                                    this.rotateLeft();
                                                                }}>
                                                                    <Icon type="reload" style={{
                                                                        transform: 'scale(-1, 1)'
                                                                    }}/>
                                                                    <div>Влево</div>
                                                                </div>
                                                                <div className="panel_item" onClick={() => {
                                                                    this.rotateRight();
                                                                }}>
                                                                    <Icon type="reload"/>
                                                                    <div>Вправо</div>
                                                                </div>
                                                                <div className="panel_item" onClick={() => {
                                                                    this._crop();
                                                                }}>
                                                                    <Icon type="save"/>
                                                                    <div>Сохранить</div>
                                                                </div>
                                                            </div>

                                                        </Col> : <>
                                                            <Col xs={4} key={'lf'}>
                                                                <div className="panel_item" onClick={() => {
                                                                    this.rotateLeft();
                                                                }}>
                                                                    <Icon type="reload" style={{
                                                                        transform: 'scale(-1, 1)'
                                                                    }}/>
                                                                    <div>Влево</div>
                                                                </div>
                                                            </Col>
                                                            <Col xs={4} key={'rt'}>
                                                                <div className="panel_item" onClick={() => {
                                                                    this.rotateRight();
                                                                }}>
                                                                    <Icon type="reload"/>
                                                                    <div>Вправо</div>
                                                                </div>
                                                            </Col>
                                                            <Col xs={4} key={'sv'}>
                                                                <div className="panel_item" onClick={() => {
                                                                    this._crop();
                                                                }}>
                                                                    <Icon type="save"/>
                                                                    <div>Сохранить</div>
                                                                </div>
                                                            </Col>
                                                        </>
                                                        }
                                                    </Row>
                                                </Grid>
                                            </div>
                                        </div>}
                                        {marked_image &&
                                        <div className="upload-page__cropper-result">
                                            <img src={'data:image/bmp;base64,' + marked_image}
                                                 alt="cropped"/>
                                        </div>}
                                    </div>
                                </div>
                            </Grid>
                        </div>

                    }
                    rightbarTitle="Загрузка фото: справка"
                    rightbar={
                        <div>
                            <div className="help">
                                <div className="help__card">
                                    Загрузите фотографию, для этого нажмите на соответсвующую кнопку и выберите
                                    фотографию
                                    из памяти телефона или сделайте новую. После этого появится форма для
                                    редактирования.
                                </div>
                                <div className="help__card">
                                    Переверните
                                    фотографию так, чтобы оттиски слева соответствовали правому глазу, а оттиски справа
                                    -
                                    левому.
                                    Обрежте фотографию так, чтобы часть с оттисками занимала большую часть фотографии.
                                </div>
                                <div className="help__card">
                                    После этого нажмите на кнопку "Подтвердить".
                                </div>
                            </div>
                        </div>
                    }/>
            </>
        );
    }
}

export default UploadPage;
