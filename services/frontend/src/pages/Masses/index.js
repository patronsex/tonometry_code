import React from "react";
import Main from "../../layout/Main";
import {inject, observer} from "mobx-react";
import {Form, Select} from "antd";
import {Redirect} from "react-router";
import {Col, Grid, Row} from "react-flexbox-grid";
import PressureCalculation from "../../components/PressureCalculation";

const {Option} = Select;

@inject("procedureStore")
@observer
class MassesPage extends React.Component {

    constructor(props) {
        super(props);

        const rows = this.props.procedureStore.eyes && this.props.procedureStore.eyes.length < 6 ? 6 : 4;

        this.state = {
            redirect: false,
            redirectTo: true,
            savedImage: null,
            rows
        };
    }

    handleSubmit = e => {
        e.preventDefault();
        const {procedureStore} = this.props;
        this.props.form.validateFields(async (err, values) => {
                if (!err) {
                    //код для модуля сожедращего элестотонометрию
                    //отправлять глазики с формы, а не с ответа Жени

                    //собираем глазики
                    const p5 = PressureCalculation.get_p5({
                        values: values,
                        rations: procedureStore.ratios
                    });

                    const p10 = PressureCalculation.get_p10({
                        values: values,
                        rations: procedureStore.ratios
                    });

                    const p15 = PressureCalculation.get_p15({
                        values: values,
                        rations: procedureStore.ratios
                    });

                    console.log(p15);

                    procedureStore.setCalculatedData({
                        p5_right: p5.right_eye,
                        p5_left: p5.left_eye,
                        p10_right: p10.right_eye,
                        p10_left: p10.left_eye,
                        p15_right: p15.right_eye,
                        p15_left: p15.left_eye
                    });

                    if (
                        (p5.left_eye.length && p5.right_eye.length) &&
                        (p10.left_eye.length && p10.right_eye.length) &&
                        (p15.left_eye.length && p15.right_eye.length)
                    ) {

                        const {
                            p5_0_l,
                            p5_0_r,
                            p10_0_l,
                            p10_0_r,
                            p15_0_l,
                            p15_0_r
                        } = PressureCalculation.get_p0_for_different_mass(p5, p10, p15);

                        const {ep_l, ep_r} = PressureCalculation.get_elastoplasticity(p15_0_l, p5_0_l, p15_0_r, p5_0_r);

                        const {p0_r, p0_l, b_l, b_r} = PressureCalculation.get_approximate_value(p5, p10, p15);

                        procedureStore.setCalculatedData({
                            p5_0_right: p5_0_r,
                            p5_0_left: p5_0_l,
                            p10_0_right: p10_0_r,
                            p10_0_left: p10_0_l,
                            p15_0_right: p15_0_r,
                            p15_0_left: p15_0_l,
                            ep_right: ep_r,
                            ep_left: ep_l,
                            p0_right: p0_r,
                            p0_left: p0_l,
                            b_r,
                            b_l
                        });

                    }

                    const next = procedureStore.getNext();
                    this.setState({redirect: true, redirectTo: next});
                }
            }
        );
    };

    componentDidMount() {
        this.props.procedureStore.setCurrentPage('/masses');
        this.props.procedureStore.validate('/masses').catch(err => {
            this.setState({redirect: true, redirectTo: err.redirectTo});
        })
    }

    render() {

        if (this.state.redirect && this.state.redirectTo) return <Redirect to={this.state.redirectTo}/>;

        const {form: {getFieldDecorator}, procedureStore} = this.props;
        const {rows} = this.state;

        const additionalMenuItems = procedureStore.getUrlsWithTitle();

        return (
            <Main
                additionalMenuItems={additionalMenuItems}
                content={
                    <div className="masses-page">
                        <div className="masses-page__form">
                            {procedureStore.marked_image && <img style={{
                                // width: "100%",
                                borderRadius: '15px',
                                height: 400
                            }} src={'data:image/bmp;base64,' + procedureStore.marked_image}
                                                                 alt="cropped"/>}
                            <p style={{
                                marginTop: '20px'
                            }}>Выберите оттиск и вес грузика:</p>
                            <Form onSubmit={this.handleSubmit}>
                                <Grid>
                                    <Row>
                                        {procedureStore.eyes && procedureStore.masses && procedureStore.eyes.map((item, key) => {
                                            return <Col xs={6} md={rows} key={key}>
                                                <Form.Item label={`Оттиск ${key + 1}`}>
                                                    {getFieldDecorator(`eye${key}`, {
                                                        rules: [{required: true, message: 'Укажите оттиск'}],
                                                        initialValue: `${procedureStore.masses[key].toString()}-${item.toString()}`
                                                    })(
                                                        <Select
                                                            placeholder="Укажите оттиск"
                                                        >
                                                            <Option value="5-0">5г - Левый оттиск</Option>
                                                            <Option value="5-1">5г - Правый оттиск</Option>
                                                            <Option value="10-0">10г - Левый оттиск</Option>
                                                            <Option value="10-1">10г - Правый оттиск</Option>
                                                            <Option value="15-0">15г - Левый оттиск</Option>
                                                            <Option value="15-1">15г - Правый оттиск</Option>
                                                            <Option value="n">Не тонометрический круг</Option>
                                                        </Select>,
                                                    )}
                                                </Form.Item>
                                            </Col>
                                        })}
                                    </Row>
                                </Grid>

                                <Grid>
                                    <button className="btn btn-blue">Отправить</button>
                                </Grid>

                            </Form>
                        </div>
                    </div>

                }
                rightbarTitle="Массы: справка"
                rightbar={
                    <div>
                        <div className="help">
                            <div className="help__card">
                                Проверьте корректность масс которые определил наш сервис
                            </div>
                        </div>
                    </div>
                }
            />
        );
    }
}

export default Form.create({name: 'masses_form'})(MassesPage);
