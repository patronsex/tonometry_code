import React from "react";
import {Redirect} from "react-router-dom";
import Main from "../../layout/Main";
import {Col, Grid, Row} from "react-flexbox-grid";
import {Descriptions, Pagination} from 'antd';
import PatientPageApi from "../../api/patient_page";

class PatientPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            redirect: false,
            redirectTo: null,
            patient_id: this.props.match.params.patient,
            page: 1,
            patient_cards: []
        };
    }

    redirect(url) {
        this.setState({redirect: true, redirectTo: url});
    }

    paginationChange(page) {
        const {patient_id} = this.state;
        this.fetchPatients(patient_id, page);
    }

    async fetchPatients(id, page) {
        const result = await PatientPageApi.getHistory({
            patient_id: id,
            page: page
        });
        if (result.length) {
            this.setState({patient_cards: result})
        }
    }

    componentDidMount() {
        const {patient_id, page} = this.state;
        this.fetchPatients(patient_id, page);
    }

    render() {
        const {redirect, redirectTo, patient_cards, page} = this.state;

        if (redirect && redirectTo) {
            return <Redirect to={redirectTo}/>
        }

        return (
            <Main
                content={
                    <div className="patient-page">
                        <div className="patient-page__categories">
                            <Grid fluid>
                                <header>
                                    <h2>История пациента
                                        №{this.props.match.params.patient ? this.props.match.params.patient : 'Ошибка'}</h2>
                                </header>
                                {patient_cards.map((card, key) => {
                                    return (
                                        <div className="site-page-header-ghost-wrapper" key={key}>
                                            <Descriptions
                                                title={`Приём №${card.session_id ? card.session_id : ' ошибка'}`}
                                                bordered>
                                                <Descriptions.Item label="Дата приёма"
                                                                   span={3}>{card.date ? card.date : 'не найдено'}</Descriptions.Item>
                                                <Descriptions.Item label="Давление"
                                                                   span={3}>{card.pressure ? card.pressure : 'не найдено'}</Descriptions.Item>
                                                <Descriptions.Item label="Качество"
                                                                   span={3}>{card.quality ? card.quality : 'не найдено'}</Descriptions.Item>
                                                <Descriptions.Item label="Диагноз"
                                                                   span={3}>{card.diagnosis ? card.diagnosis : 'не найдено'}</Descriptions.Item>
                                                <Descriptions.Item label="Назначеное лечение"
                                                                   span={3}>{card.therapy ? card.therapy : 'не найдено'}</Descriptions.Item>
                                                <Descriptions.Item label="Рекомендация"
                                                                   span={3}>{card.recommendations ? card.recommendations : 'не найдено'}</Descriptions.Item>
                                            </Descriptions>
                                        </div>
                                    )
                                })}


                                <br/>
                                <Row end="xs">
                                    <Col xs={6}>
                                        <Pagination defaultCurrent={page} total={500}
                                                    onChange={this.paginationChange.bind(this)}/>
                                    </Col>
                                </Row>

                            </Grid>
                        </div>
                    </div>
                }
                rightbarTitle="Выбор категории: справка"
                rightbar={
                    <div>
                        <div className="help">
                            <div className="help__card">
                                На карте пациентов представлена история сессий
                            </div>
                        </div>
                    </div>
                }
            />
        );
    }
}

export default PatientPage;