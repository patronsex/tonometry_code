import React from "react";
import {Form, Select} from "antd";
import {Grid, Row, Col} from "react-flexbox-grid";

import MobileSelectLayout from "../../layout/MobileSelectLayout";

const {Option} = Select;

class PatientForm extends React.Component {

    state = {
        redirect: false,
        redirectTo: null,
        showMedicalLeft: false,
        showMedicalRight: false,
        values_left: [],
        values_right: [],
        category_right: ["Не получает лечения", "Простагландины", "Бета блокаторы", "Адреномиметики", "Ингибиторы карбоангидразы", "М-холиномиметик", "Ретинопротекция"],
        category_left: ["Не получает лечения", "Простагландины", "Бета блокаторы", "Адреномиметики", "Ингибиторы карбоангидразы", "М-холиномиметик", "Ретинопротекция"]

    };

    onChangeLeft = value => {
        if (value.indexOf("Не получает лечения") + 1) {
            this.setState({
                category_left: ["Не получает лечения"],
                values_left: ["Не получает лечения"]
            }, () => {
                this.props.form.setFieldsValue({
                    'medication_left': ["Не получает лечения"]
                });
            });
        } else {
            this.setState({
                category_left: ["Не получает лечения", "Простагландины", "Бета блокаторы", "Адреномиметики", "Ингибиторы карбоангидразы", "М-холиномиметик", "Ретинопротекция"],
                values_left: value
            }, () => {
                this.props.form.setFieldsValue({
                    'medication_left': value
                });
            })
        }
    };

    onChangeRight = value => {
        if (value.indexOf("Не получает лечения") + 1) {
            this.setState({
                category_right: ["Не получает лечения"],
                values_right: ["Не получает лечения"]
            }, () => {
                this.props.form.setFieldsValue({
                    'medication_right': ["Не получает лечения"]
                });
            });
        } else {
            this.setState({
                category_right: ["Не получает лечения", "Простагландины", "Бета блокаторы", "Адреномиметики", "Ингибиторы карбоангидразы", "М-холиномиметик", "Ретинопротекция"],
                values_right: value
            }, () => {
                this.props.form.setFieldsValue({
                    'medication_right': value
                });
            })
        }
    };


    handleSubmit = e => {
        e.preventDefault();

        const {onSubmit} = this.props;

        this.props.form.validateFields((err, values) => {
            if (!err) {
                if (typeof onSubmit === "function") {
                    onSubmit(values);
                }
            }
        });
    };

    render() {

        const {getFieldDecorator} = this.props.form;
        const {width} = this.props;

        return (
            <Form onSubmit={this.handleSubmit} className="prepations_page__form">
                <Grid>
                    <Row>
                        <Col xs={12}>
                            <Form.Item label="Стадия глаукомы у пациента (левый глаз):">
                                {getFieldDecorator('stage_left', {
                                    rules: [{required: true, message: 'Укажите стадию глаукомы'}],
                                })(
                                    <Select placeholder="Выберите...">
                                        <Option value="0">Подозрение на глаукому</Option>
                                        <Option value="1">I Стадия глаукомы (начальная)</Option>
                                        <Option value="2">II Стадия глаукомы (развитая)</Option>
                                        <Option value="3">III - IV Стадия глаукомы (далеко зашедшая и
                                            терминальная)</Option>

                                    </Select>
                                )}
                            </Form.Item>

                            <Form.Item label="Стадия глаукомы у пациента (правый глаз):">
                                {getFieldDecorator('stage_right', {
                                    rules: [{required: true, message: 'Укажите стадию глаукомы'}],
                                })(
                                    <Select placeholder="Выберите...">
                                        <Option value="0">Подозрение на глаукому</Option>
                                        <Option value="1">I Стадия глаукомы (начальная)</Option>
                                        <Option value="2">II Стадия глаукомы (развитая)</Option>
                                        <Option value="3">III - IV Стадия глаукомы (далеко зашедшая и
                                            терминальная)</Option>

                                    </Select>
                                )}
                            </Form.Item>

                            <div
                                className="form_mobile_container_hidden">
                                {width < 500 && <div className="mobile-overlay" onClick={() => {
                                    this.setState({showMedicalLeft: true});
                                }}/>}

                                {this.state.showMedicalLeft && <MobileSelectLayout
                                    onCancel={() => {
                                        this.setState({showMedicalLeft: false});
                                    }}
                                    description="Какое лечение получает пациент ?"
                                    show={true}
                                    data={this.state.category_left}
                                    selected={this.state.values_left}
                                    onChange={(value) => {
                                        this.onChangeLeft(value);
                                    }}
                                    onSave={(value) => {
                                        this.onChangeLeft(value);
                                    }}
                                />}

                                <Form.Item label="Какое лечение получает пациент (левый глаз):">
                                    {getFieldDecorator('medication_left', {
                                        rules: [{required: true, message: 'Укажите препарат'}],
                                        // initialValue: initialDrugs
                                    })(
                                        <Select
                                            mode="multiple"
                                            // value={this.state.value}
                                            placeholder="Выберите..."
                                            onChange={this.onChangeLeft}
                                        >

                                            {this.state.category_left.map((item, key) => {
                                                return <Option value={item} key={key}>{item}</Option>
                                            })}

                                        </Select>
                                    )}
                                </Form.Item>
                            </div>

                            <div className="form_mobile_container_hidden">
                                {width < 500 && <div className="mobile-overlay" onClick={() => {
                                    this.setState({showMedicalRight: true});
                                }}/>}

                                {this.state.showMedicalRight && <MobileSelectLayout
                                    onCancel={() => {
                                        this.setState({showMedicalRight: false});
                                    }}
                                    description="Какое лечение получает пациент ?"
                                    show={true}
                                    data={this.state.category_right}
                                    selected={this.state.values_right}
                                    onChange={(value) => {
                                        this.onChangeRight(value);
                                    }}
                                    onSave={(value) => {
                                        this.onChangeRight(value);
                                    }}
                                />}

                                <Form.Item label="Какое лечение получает пациент (правый глаз):">
                                    {getFieldDecorator('medication_right', {
                                        rules: [{required: true, message: 'Укажите препарат'}],
                                        // initialValue: initialDrugs
                                    })(
                                        <Select
                                            mode="multiple"
                                            // value={this.state.value}
                                            placeholder="Выберите..."
                                            onChange={this.onChangeRight}
                                        >

                                            {this.state.category_right.map((item, key) => {
                                                return <Option value={item} key={key}>{item}</Option>
                                            })}

                                        </Select>
                                    )}
                                </Form.Item>
                            </div>

                        </Col>
                    </Row>
                </Grid>
                <Form.Item>
                    <Grid>
                        <label className="ant-form-item-required"
                               title="Какое лечение получает пациент">Дата рождения</label>
                        <Row xs={12}>
                            <Col xs={4}>
                                <Form.Item>
                                    {getFieldDecorator('day', {
                                        rules: [{required: true, message: 'Укажите день'}],
                                    })(
                                        <Select
                                            placeholder="День"
                                        >
                                            {Array.from(Array(31).keys()).map(item => {
                                                return <Option value={item + 1} key={item + 1}>{item + 1}</Option>
                                            })}
                                        </Select>
                                    )}
                                </Form.Item>
                            </Col>
                            <Col xs={4}>
                                <Form.Item>
                                    {getFieldDecorator('month', {
                                        rules: [{required: true, message: 'Укажите месяц'}],
                                    })(
                                        <Select
                                            placeholder="Месяц"
                                        >
                                            <Option value={1}>Январь</Option>
                                            <Option value={2}>Фервраль</Option>
                                            <Option value={3}>Март</Option>
                                            <Option value={4}>Апрель</Option>
                                            <Option value={5}>Май</Option>
                                            <Option value={6}>Июнь</Option>
                                            <Option value={7}>Июль</Option>
                                            <Option value={8}>Август</Option>
                                            <Option value={9}>Сентябрь</Option>
                                            <Option value={10}>Октябрь</Option>
                                            <Option value={11}>Ноябрь</Option>
                                            <Option value={12}>Декабрь</Option>
                                        </Select>
                                    )}
                                </Form.Item>
                            </Col>
                            <Col xs={4}>
                                <Form.Item>
                                    {getFieldDecorator('year', {
                                        rules: [{required: true, message: 'Укажите год'}],
                                    })(
                                        <Select
                                            placeholder="Год"
                                        >
                                            {Array.from(Array(95).keys()).map(item => {
                                                return <Option value={item + 1920}
                                                               key={item + 1920}>{item + 1920}</Option>
                                            })}
                                        </Select>
                                    )}
                                </Form.Item>
                            </Col>
                        </Row>
                    </Grid>
                </Form.Item>
                <Grid>
                    <input type="submit" className="btn btn-blue" value="Сохранить"/>
                </Grid>
            </Form>
        );
    }
}

export default Form.create({name: "patient_form"})(PatientForm);
