import React from "react";
import Main from "../../layout/Main";
import {Grid, Row, Col} from "react-flexbox-grid";
import PatientForm from "./PatientForm";
import {inject, observer} from "mobx-react";
import {Redirect} from "react-router";

@inject("procedureStore")
@observer
class PreparationsPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            redirect: null,
            redirectTo: null,
            width: window.innerWidth,
            height: window.innerHeight
        }
    }

    updateDimensions = () => {
        this.setState({width: window.innerWidth, height: window.innerHeight});
    };

    componentDidMount() {
        window.addEventListener('resize', this.updateDimensions);
        this.props.procedureStore.setCurrentPage('/patient');
        this.props.procedureStore.validate('/patient').catch(err => {
            this.setState({redirect: true, redirectTo: err.redirectTo});
        });
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateDimensions);
    }

    render() {

        const {width, redirect, redirectTo} = this.state;
        const {procedureStore} = this.props;

        if (redirect && redirectTo) return <Redirect to={redirectTo}/>;

        const additionalMenuItems = procedureStore.getUrlsWithTitle();

        return (
            <Main
                additionalMenuItems={additionalMenuItems}
                content={
                    <div className="preparations-page">
                        <div className="preparations-page__forms">
                            <Grid fluid>
                                <Row>
                                    <Col xs={12} md={6}>
                                        <PatientForm width={width} onSubmit={values => {
                                            procedureStore.setPatientInfo(values);
                                            const next = procedureStore.getNext();
                                            this.setState({redirect: true, redirectTo: next});
                                        }}/>
                                    </Col>
                                </Row>
                            </Grid>
                        </div>
                    </div>
                }
                rightbarTitle="О пациенте: справка"
                rightbar={
                    <div>
                        <div className="help">
                            <div className="help__card">
                                Введите следующую информацию о пациенте: стадия глаукомы, какое лечение получает
                                пациент и его
                                день рождения. Эта информация поможет нам более корректно подобрать лечение.
                            </div>
                            <div className="help__card">
                                Вся введенная вами информация будет обезличена, с целью соблюдения закона о
                                персональных данных.
                            </div>
                        </div>
                    </div>
                }/>
        );
    }
}

export default PreparationsPage;