import React from "react";
import Main from "../../layout/Main";
import {Grid, Col, Row} from "react-flexbox-grid";
import {Link, Redirect} from "react-router-dom";
import {inject, observer} from "mobx-react";
import Card from "../../components/Card";
import {Icon} from "antd";


@inject("procedureStore")
@observer
class MainPage extends React.Component {

    state = {
        redirect: false,
        redirectTo: null
    };

    render() {


        const {redirect, redirectTo} = this.state;
        const {procedureStore} = this.props;

        if (redirect && redirectTo) {
            return <Redirect to={redirectTo}/>
        }

        return (
            <Main
                stage={1}
                content={
                    <div className="main-page">
                        <div className="main-page__categories">
                            <Grid fluid>
                                <Row className="main-page__cards">
                                    <Col xs={12} xl={5} md={6} className="card__wrapper">
                                        <Card
                                            cardTitle="Начните работу"
                                            icon={<Icon type="snippets" theme="filled"/>}
                                            description="Используйте этот модуль при проведении измерения внутриглазного давления
                        тонометрией по Маклакову. Этот метод предполагает использование
                        тонометров
                        массой 10 грамм."
                                            actions={<>
                                                <Link to="/upload"
                                                      onClick={() => procedureStore.setProcedure("default_procedure")}>
                                                    <button className="btn btn-blue">Первичный прием</button>
                                                </Link>
                                                {/*<Link to="/history">*/}
                                                {/*    <button className="btn blue-bordered">*/}
                                                {/*        Повторный прием*/}
                                                {/*    </button>*/}
                                                {/*</Link>*/}
                                            </>}
                                        />
                                    </Col>
                                </Row>
                            </Grid>
                        </div>
                    </div>
                }
                rightbarTitle="Выбор категории: справка"
                rightbar={
                    <div>
                        <div className="help">
                            <div className="help__card">
                                Мы представляем Вам автоматизированный инструмент разметки тонограмм. Для начала работы
                                выберите тип процедуры.
                            </div>
                        </div>
                    </div>
                }
            />
        );
    }
}

export default MainPage;
