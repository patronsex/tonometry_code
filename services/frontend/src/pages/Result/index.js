import React from "react";
import Main from "../../layout/Main";
import {inject, observer} from "mobx-react";
import {Redirect} from "react-router";
import Plot from 'react-plotly.js';
import {Grid, Col, Row} from "react-flexbox-grid";

@inject("procedureStore")
@observer
class ResultPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            redirect: false,
            redirectTo: null,
            width: window.innerWidth,
            height: window.innerHeight
        }
    }

    updateDimensions = () => {
        this.setState({width: window.innerWidth, height: window.innerHeight});
    };

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateDimensions);
    }

    componentDidMount() {
        window.addEventListener('resize', this.updateDimensions);
        this.props.procedureStore.setCurrentPage('/result');
        //     if (res.rec_drug === "drug_PG") message += "Рекомендуем перейти с текущего лечения на простагландины.";
        //     if (res.rec_drug === "drug_PG_BB") message += "Рекомендуем перейти с текущего лечения на комбенацию простагландины + бета блокаторы.";
        //     if (res.add_drug === "drug_PG") message += "Рекомендуем добавить к текущему лечению простагландины.";
        //     if (res.add_drug === "drug_PG_BB") message += "Рекомендуем добавить к текущему лечению комбинацию простагландины + бета блокаторы.";
        //     if (res.add_drug === "drug_PG_BB, drug_AM") message += "Рекомендуем добавить к текущему лечению комбинацию простагландины + бета блокаторы и адреномиметики.";
        //     if (res.add_drug === "drug_AM") message += "Рекомендуем добавить к текущему лечению комбинацию адреномиметики.";
        //     if (res.add_drug === "drug_IKA") message += "Рекомендуем добавить к текущему лечению ингибиторы карбоангидразы.";
        //     if (res.RP === "1") message += "Необходимо назначить ретинопротекцию.";
        //     if (res.time === "3 month") message += "Назначить повторный осмотр через 3 месяца.";
        //     if (res.time === "2 week") message += "Назначить повторный осмотр через 2 недели.";
        //     if (res.diagnostic === "VGD") message += "Необходим контроль внутриглазного давления.";
        //     if (res.diagnostic === "operation") message += "Необходимо готовить пациента к оперативному лечению.";
        //     if (res.diagnostic === "field and OKT") message += "Необходим контроль полей зрения и проведение оптической когерентной томографии.";
        //     if (
        //         res.time === 0 &&
        //         res.rec_drug === 0 &&
        //         res.diagnostic === 0 &&
        //         res.add_drug === 0 &&
        //         res.RP === 0
        //     ) {
        //         message += "Пациент здоров";
        //     }

    }

    render() {

        if (this.state.redirect && this.state.redirectTo)
            return <Redirect to={this.state.redirectTo}/>;

        const {procedureStore} = this.props;

        const additionalMenuItems = procedureStore.getUrlsWithTitle();

        const {p0_right, b_r, p0_left, b_l, p5_left, p5_right, p10_left, p10_right, p15_left, p15_right } = procedureStore;

        const prepareLeftPoints5 = p5_left && {
            x: Array.from({length: p5_left.length}, () => 5),
            y: Array.from(p5_left),
            type: 'scatter',
            mode: 'markers',
            marker: {color: 'blue'},
        };

        const prepareRightPoints5 = p5_right && {
            x: Array.from({length: p5_right.length}, () => 5),
            y: Array.from(p5_right),
            type: 'scatter',
            mode: 'markers',
            marker: {color: 'green'},
        };

        const prepareLeftPoints10 = p10_left && {
            x: Array.from({length: p10_left.length}, () => 10),
            y: Array.from(p10_left),
            type: 'scatter',
            mode: 'markers',
            marker: {color: 'blue'},
        };

        const prepareRightPoints10 = p10_right && {
            x: Array.from({length: p10_right.length}, () => 10),
            y: Array.from(p10_right),
            type: 'scatter',
            mode: 'markers',
            marker: {color: 'green'},
        };

        const prepareLeftPoints15 = p15_left && {
            x: Array.from({length: p15_left.length}, () => 15),
            y: Array.from(p15_left),
            type: 'scatter',
            mode: 'markers',
            marker: {color: 'blue'},
        };

        const prepareRightPoints15 = p15_right && {
            x: Array.from({length: p15_right.length}, () => 15),
            y: Array.from(p15_right),
            type: 'scatter',
            mode: 'markers',
            marker: {color: 'green'},
        };

        // console.log(p15_left, p15_right, p5_left, p5_right);

        console.log(procedureStore);

        return (
            <Main
                additionalMenuItems={additionalMenuItems}
                content={
                    <div className="result_page">
                        <div className="result_page__form">
                            <Grid fluid>
                                <Row>
                                    <Col xs={12}>
                                        <div style={{
                                            margin: '0 -16px',
                                            overflow: 'hidden'
                                        }}>
                                            {procedureStore.marked_image && <img style={{
                                                // width: "100%",
                                                borderRadius: '15px',
                                                height: 400,
                                                marginBottom: '16px'
                                            }} src={'data:image/bmp;base64,' + procedureStore.marked_image}
                                                                                 alt="cropped"/>}
                                        </div>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs={6}>
                                        <div style={{
                                            margin: '0 -16px'
                                        }}>
                                            {p0_left && b_l && <Plot
                                                data={[
                                                    {
                                                        x: [0, 15],
                                                        y: [p0_left, b_l * 15 + p0_left],
                                                        type: 'scatter',
                                                        mode: 'lines+markers',
                                                        marker: {color: 'red'},
                                                    },
                                                    prepareLeftPoints5,
                                                    prepareLeftPoints10,
                                                    prepareLeftPoints15
                                                ]}
                                                layout={ {width: (this.state.width - 540) / 2, height: 400, title: 'ВГД OS'} }
                                            />}
                                            <h3>Левый глаз</h3>
                                            <table style={{
                                                width: '100%'
                                            }}>
                                                <thead>
                                                <tr>
                                                    <th>5 грамм</th>
                                                    <th>10 грамм</th>
                                                    <th>15 грамм</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>{p5_left && p5_left[0] && p5_left[0].toFixed(3)}</td>
                                                    <td>{p10_left && p10_left[0] && p10_left[0].toFixed(3)}</td>
                                                    <td>{procedureStore.p15_left && procedureStore.p15_left[0] && procedureStore.p15_left[0].toFixed(3)}</td>
                                                </tr>
                                                <tr>
                                                    <td>{p5_left && p5_left[1] && p5_left[1].toFixed(3)}</td>
                                                    <td>{p10_left && p10_left[1] && p10_left[1].toFixed(3)}</td>
                                                    <td>{procedureStore.p15_left && procedureStore.p15_left[1] && procedureStore.p15_left[1].toFixed(3)}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </Col>
                                    <Col xs={6}>
                                        <div style={{
                                            margin: '0 -16px',
                                            marginBottom: '16px'
                                        }}>
                                            {p0_right && b_r && <Plot
                                                data={[
                                                    {
                                                        x: [0, 15],
                                                        y: [p0_right, b_r * 15 + p0_right],
                                                        type: 'scatter',
                                                        mode: 'lines+markers',
                                                        marker: {color: 'red'},
                                                    },
                                                    prepareRightPoints5,
                                                    prepareRightPoints10,
                                                    prepareRightPoints15
                                                ]}
                                                layout={ {width: (this.state.width - 540) / 2, height: 400, title: 'ВГД OD'} }
                                            />}
                                            <h3>Правый глаз</h3>
                                            <table style={{
                                                width: '100%'
                                            }}>
                                                <thead>
                                                <tr>
                                                    <th>5 грамм</th>
                                                    <th>10 грамм</th>
                                                    <th>15 грамм</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>{p5_right && p5_right[0] && p5_right[0].toFixed(3)}</td>
                                                    <td>{p10_right && p10_right[0] && p10_right[0].toFixed(3)}</td>
                                                    <td>{p15_right && p15_right[0] && p15_right[0].toFixed(3)}</td>
                                                </tr>
                                                <tr>
                                                    <td>{p5_right && p5_right[1] && p5_right[1].toFixed(3)}</td>
                                                    <td>{p10_right && p10_right[1] && p10_right[1].toFixed(3)}</td>
                                                    <td>{p15_right && p15_right[1] && p15_right[1].toFixed(3)}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </Col>
                                </Row>
                            </Grid>
                            <div>
                                <h2>Обратная связь: </h2>

                                <p>Вы в числе первых, кто опробовал наш сервис. Пожалуйста, дайте нам обратную связь,
                                    расскажите о ваших впечатлениях или идеях и пожеланиях.
                                    Оставьте любые контактные данные и мы расскажем вам, когда ждать обновлений и сможем
                                    ли мы внедрить предложенные вами решения.</p>

                                <div style={{
                                    marginTop: 20
                                }}>
                                    Вы можете связаться с нами:
                                    <div style={{
                                        marginTop: 10,
                                        marginBottom: 10
                                    }}>
                                        <a href="mailto:gorobets@ai-tonometry.com">gorobets@ai-tonometry.com - Александр
                                            Горобец, head of
                                            development</a>
                                    </div>
                                    <div style={{
                                        marginBottom: 10
                                    }}>
                                        <a href="mailto:vasilenko@ai-tonometry.com">vasilenko@ai-tonometry.com -
                                            Василенко Денис, программист</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                }
            />
        )
    }
}

export default ResultPage;
