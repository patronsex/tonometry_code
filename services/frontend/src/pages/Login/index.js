import React from "react";
import {Redirect} from "react-router";
import Loader from "../../components/Loader";
import {Alert, Button, Modal} from "antd";
import {logout, userIsLogged, authorize} from "../../helpers/auth.js";

class LoginForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            redirect: false,
            redirectTo: null,
            alreadyLogged: false,
            load: true,
            code: this.findGetParameter('code')
        };
    }

    findGetParameter(parameterName) {
        let result = null,
            tmp = [];
        const sub_str = window.location.search.substr(1);
        let items = sub_str ? sub_str.split("&") : null;
        if (!items) return null;
        for (let index = 0; index < items.length; index++) {
            tmp = items[index].split("=");
            if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        }
        return result;
    }

    redirect(to) {
        this.setState({
            redirect: true,
            redirectTo: to
        });
    }

    componentDidMount() {
        const {code} = this.state;
        if (!userIsLogged()) {
            authorize(code)
                .then(() => {
                    this.redirect('/');
                })
                .catch(() => {
                    logout();
                });
        } else {
            this.setState({alreadyLogged: true, load: false})
        }
    }

    render() {
        const {redirect, redirectTo, alreadyLogged, load} = this.state;
        if (redirect && redirectTo) return <Redirect to={redirectTo}/>;
        return (
            <div className="login-page">
                <Loader show={load}/>
                <Modal
                    title="Предупреждение"
                    visible={alreadyLogged}
                    onCancel={() => {
                        this.redirect('/')
                    }}
                    footer={[
                        <Button key="back" onClick={() => {
                            logout();
                        }}>
                            Выйти
                        </Button>,
                        <Button key="submit" type="primary" onClick={() => {
                            this.redirect('/')
                        }}>
                            Перейти на сайт
                        </Button>,
                    ]}
                >
                    <Alert message="Вы уже авторизованы" type="warning"/>
                </Modal>
            </div>
        );
    }
}

export default LoginForm;
