import React from "react";
import {Icon, Popover, Select} from 'antd';

const {Option} = Select;

const content = (
    <div>
        Какой-то help
    </div>
);

export default class Filters extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            patient_ids: [],
            doctor_ids: []
        };
    }

    fetchPatientIds = () => {
        //запрос на id пациентов
        this.setState({patient_ids: ['ID:20, Вася Иванов', 'ID:21, Вася Петров', 'ID:22, Вася Андреев']});
    };

    fetchDoctorIds = () => {
        // запрос на id докторов
        this.setState({doctor_ids: ['ID:20, Вася Иванов', 'ID:21, Вася Петров', 'ID:22, Вася Андреев']});
    };

    componentDidMount() {
        this.fetchPatientIds();
        this.fetchDoctorIds();
    }

    render() {

        const {patient_ids, doctor_ids} = this.state;

        return <div className="filters">
            <div className="filter-item">
                <Select
                    showSearch
                    style={{width: '100%'}}
                    placeholder="ID или ФИО пациента"
                    optionFilterProp="children"
                    filterOption={(input, option) =>
                        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                    }
                >
                    {patient_ids && patient_ids.map((item, key) => {
                        return <Option value={item} key={key}>{item}</Option>;
                    })}
                </Select>
                <Popover content={content} placement="left" title="Title">
                    <Icon type="question-circle"/>
                </Popover>
            </div>
            <div className="filter-item">
                <Select
                    mode="tags"
                    showSearch
                    style={{width: '100%'}}
                    placeholder="ID или ФИО врача"
                    optionFilterProp="children"
                    filterOption={(input, option) =>
                        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                    }
                >
                    {doctor_ids && doctor_ids.map((item, key) => {
                        return <Option value={item} key={key}>{item}</Option>;
                    })}
                </Select>
                <Popover content={content} placement="left" title="Title">
                    <Icon type="question-circle"/>
                </Popover>
            </div>
        </div>
    }

}