import React from "react";
import {Redirect} from "react-router-dom";
import Main from "../../layout/Main";
import {Col, Grid, Row} from "react-flexbox-grid";
import {Pagination} from 'antd';
import HistoryApi from "../../api/history";
import PatientCard from "./PatientCard.js";
import {loginIfNotAuthorized} from "../../helpers/auth";
import Filters from "./Filters";

class History extends React.PureComponent {

    state = {
        redirect: false,
        redirectTo: null,
        patient_cards: [],
        page: 1
    };

    async componentDidMount() {
        loginIfNotAuthorized();
        const {result} = await HistoryApi.getHistory({
            doctor_id: 20
        });
        if (result.length) {
            this.setState({patient_cards: result})
        }
    }

    render() {
        const {redirect, redirectTo, patient_cards} = this.state;

        if (redirect && redirectTo) {
            return <Redirect to={redirectTo}/>
        }

        return (
            <Main
                stage={1}
                content={
                    <div className="history-page">
                        <div className="history-page__categories">
                            <Grid fluid>
                                <header>
                                    <h2>Карты пациентов</h2>
                                </header>
                                {patient_cards.map((card, key) => {
                                    const diagnosis = JSON.parse(card.diagnosis);

                                    return <PatientCard
                                        key={key}
                                        doctor_id={card.doctor_id ? card.doctor_id : 'Доктор не назначен'}
                                        patient_id={card.patient_id}
                                        first_appointment={card.first_reception ? card.first_reception.split('T')[0] : 'не проводился'}
                                        last_appointment={card.last_reception ? card.last_reception.split('T')[0] : 'не проводился'}
                                        diagnosis={diagnosis && Object.keys(diagnosis).length ? card.diagnosis : 'не поставлен'}
                                        treatment={card.therapy ? card.therapy : 'не назначено'}
                                        more_link={`/patient_page/${card.patient_id}`}
                                        patient_label="No"
                                    />;

                                })}


                                <br/>
                                <Row end="xs">
                                    <Col xs={6}>
                                        <Pagination defaultCurrent={1} total={500}/>
                                    </Col>
                                </Row>

                            </Grid>
                        </div>
                    </div>
                }
                rightbarTitle="Карты пациентов: фильтры"
                rightbar={
                    <div>
                        <Filters/>
                    </div>
                }
            />
        );
    }
}

export default History;