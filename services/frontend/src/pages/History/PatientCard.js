import React from "react";
import {Col, Grid, Row} from "react-flexbox-grid";
import {Link} from "react-router-dom";

export default function PatientCard(props) {

    const {
        doctor_id,
        first_appointment,
        last_appointment,
        diagnosis,
        treatment,
        more_link,
        patient_id,
        patient_label
    } = props;

    return <div className="history-card">
        <div className="history-card__content-wrapper">
            <div className="history-card__patient-id">
                ID: {patient_id}
                {patient_label && <p className="history-card__patient-label">
                    {patient_label}
                </p>}
            </div>
            <div className="history-card__content">
                <Grid fluid>
                    <Row>
                        <Col xs={12} xl={4}>
                            <h3>Общая информация</h3>
                            <Row>
                                <Col xs={4} md={4} xl={12}>
                                    <div className="parameter">
                                        <p className="parameter__description">ID
                                            доктора: </p>
                                        <span className="parameter__value">{doctor_id}</span>
                                    </div>
                                </Col>
                                <Col xs={8} md={4} xl={12}>
                                    <div className="parameter">
                                        <p className="parameter__description">Дата
                                            первичного
                                            приема:</p>
                                        <span className="parameter__value">{first_appointment}</span>
                                    </div>
                                </Col>
                                <Col xs={12} md={4} xl={12}>
                                    <div className="parameter">
                                        <p className="parameter__description">Дата
                                            последнего
                                            приема:</p>
                                        <span className="parameter__value">{last_appointment}</span>
                                    </div>
                                </Col>
                            </Row>
                        </Col>
                        <Col xs={12} xl={4}>
                            <h3>Лечение</h3>
                            <Row>
                                <Col xs={6} md={4} xl={12}>
                                    <div className="parameter">
                                        <p className="parameter__description">Диагноз</p>
                                        <span className="parameter__value">{diagnosis}</span>
                                    </div>
                                </Col>
                                <Col xs={6} md={4} xl={12}>
                                    <div className="parameter">
                                        <p className="parameter__description">Получаемое лечение</p>
                                        <span className="parameter__value">{treatment}</span>
                                    </div>
                                </Col>
                                <Col xs={12} md={4} xl={12}>
                                    <div className="action">
                                        <Link to={more_link}>
                                            <button className="more_button">
                                                Подробне
                                            </button>
                                        </Link>
                                    </div>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Grid>
            </div>
        </div>
    </div>
}