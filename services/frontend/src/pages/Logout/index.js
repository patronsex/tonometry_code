import React from "react";
import {Redirect} from "react-router";
import {logout as logoutHelper} from "../../helpers/auth";

export default function logout() {
    logoutHelper();
    return <Redirect to="/"/>
}