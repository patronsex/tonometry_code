import axios from "axios";
import config from "../../../config/production.json";

const {frontend: {backend_url}} = config;

export default class PatientPageApi {
    static async getHistory(payload) {
        return await axios.post(`${backend_url}/api/v1/history/patient`, {
            patient_id: payload.patient_id,
            page: payload.page
        }).then(value => value.data).catch(error => error);
    }
}