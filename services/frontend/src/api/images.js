import config from "../../../config/production.json";
import {authorizedRequest} from "../helpers/request";

const {frontend: {backend_url}} = config;

export default class ImagesApi {
    static async sendCroppedFile(file) {
        return await authorizedRequest(`${backend_url}/api/v1/circles`, 'POST', {
            image: file.split(',')[1]
        });
    }
}