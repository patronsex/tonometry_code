import axios from "axios";
import config from "../../../config/production.json";

const {frontend: {backend_url}} = config;

export default class HistoryApi {
    static async getHistory(payload) {
        return await axios.post(`${backend_url}/api/v1/history/get`, {
            doctor_id: payload.doctor_id
        }).then(value => value.data).catch(error => error);
    }
}