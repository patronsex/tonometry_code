const {Sequelize, Model, DataTypes} = require('sequelize');

module.exports = async ({database, login, password, host, port} = {}) => {
    const sequelize = new Sequelize(database, login, password, {
        dialect: "mysql",
        host: host,
        port: port
    });

    class Groups extends Model {}

    Groups.init({
        group_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        group_name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        group_creator: {
            type: DataTypes.INTEGER,
        }
    }, {sequelize, modelName: 'groups'});

    return {
        sync: async () => {
            try {
                await sequelize.sync()
            } catch (e) {
                throw e;
            }
        },
        groups: Groups
    }
};
