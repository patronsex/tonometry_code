const fs = require('fs');
const path = require('path');
const config = require('../config/production.json');

const readDirFileName = (dirPath, options = {}) => {
    const stat = fs.statSync(dirPath);
    if (!stat.isDirectory()) {
        return dirPath
    }
    const dirs = fs.readdirSync(dirPath)
        .filter(item => {
            if (options.ignore) {
                return !item.startsWith('.')
                    && item !== options.ignore
                    && item !== "package.json"
                    && item !== "package-lock.json"
                    && item !== "index.js"
                    && item !== "test.js"
            }
            return !item.startsWith('.')
        });
    return [].concat(...dirs.map(sub => readDirFileName(`${dirPath}/${sub}`)));
};

module.exports = {
    sync: async () => {
        const filePath = path.resolve(__dirname);
        const names = readDirFileName(filePath, {ignore: 'node_modules'});
        const models = await Promise.all(names.map(name => require(name)({
            database: config.database.database,
            login: config.database.user,
            password: config.database.password,
            host: config.database.host,
            port: config.database.port
        })));

        models.map(async model => {
            await model.sync();
        });
    },
    models: () => {
        const filePath = path.resolve(__dirname);
        const names = readDirFileName(filePath, {ignore: 'node_modules'});

        return Promise.all(names.map(name => require(name)({
            database: config.database.database,
            login: config.database.user,
            password: config.database.password,
            host: config.database.host,
            port: config.database.port
        }))).then(res => {
            return res.map(res => {
                return res[Object.keys(res)[1]];
            });
        });
    }
};
