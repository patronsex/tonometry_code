const {Sequelize, Model, DataTypes} = require('sequelize');

module.exports = async ({database, login, password, host, port} = {}) => {
    const sequelize = new Sequelize(database, login, password, {
        dialect: "mysql",
        host: host,
        port: port
    });

    class History extends Model {}

    History.init({
        session_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        doctor_ip: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: '0.0.0.0'
        },
        patient_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        pressure: {
            type: DataTypes.JSON,
            allowNull: false
        },
        quality: {
            type: DataTypes.JSON,
            allowNull: false
        },
        diagnosis: {
            type: DataTypes.JSON,
            allowNull: false
        },
        therapy: {
            type: DataTypes.JSON,
            allowNull: false
        },
        recommendations: {
            type: DataTypes.JSON,
            allowNull: false
        },
    }, {sequelize, modelName: 'history'});

    return {
        sync: async () => {
            try {
                await sequelize.sync()
            } catch (e) {
                throw e;
            }
        },
        history: History
    }
};
