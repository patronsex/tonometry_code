const {Sequelize, Model, DataTypes} = require('sequelize');

module.exports = async ({database, login, password, host, port} = {}) => {
    const sequelize = new Sequelize(database, login, password, {
        dialect: "mysql",
        host: host,
        port: port
    });

    class Drugs extends Model {}

    Drugs.init({
        grud_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        pg: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        am: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        ika: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        bb: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        rp: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        meta: {
            type: DataTypes.JSON,
            allowNull: true,
            defaultValue: null
        },
    }, {sequelize, modelName: 'drugs'});

    return {
        sync: async () => {
            try {
                await sequelize.sync()
            } catch (e) {
                throw e;
            }
        },
        drugs: Drugs
    }
};
