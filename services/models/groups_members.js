const {Sequelize, Model, DataTypes} = require('sequelize');

module.exports = async ({database, login, password, host, port} = {}) => {
    const sequelize = new Sequelize(database, login, password, {
        dialect: "mysql",
        host: host,
        port: port
    });

    class GroupsMembers extends Model {}

    GroupsMembers.init({
        member_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        doctor_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        group_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        access: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: true
        }
    }, {sequelize, modelName: 'groups_members'});

    return {
        sync: async () => {
            try {
                await sequelize.sync()
            } catch (e) {
                throw e;
            }
        },
        groupsMembers: GroupsMembers
    }
};
