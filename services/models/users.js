const {Sequelize, Model, DataTypes} = require('sequelize');

module.exports = async ({database, login, password, host, port} = {}) => {
    const sequelize = new Sequelize(database, login, password, {
        dialect: "mysql",
        host: host,
        port: port
    });

    class Users extends Model {}

    Users.init({
        doctor_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        password: {
            type: DataTypes.STRING(4096),
            allowNull: false
        },
        refresh_token: {
            type: DataTypes.TEXT
        },
        active: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: true
        }
    }, {sequelize, modelName: 'users'});

    return {
        sync: async () => {
            try {
                await sequelize.sync()
            } catch (e) {
                throw e;
            }
        },
        user: Users
    }
};
