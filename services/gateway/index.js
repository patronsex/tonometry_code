const fastify = require('fastify')({
    logger: true
});
const config = require('../config/production');
const {withAuth} = require('./helpers/auth.js');
// const groupRoutes = require('./modules/group');


const start = async () => {

    await fastify.register(require('fastify-cors'));
    await fastify.register(require('fastify-formbody'));
    await fastify.register(require('fastify-file-upload'));
    //интеграци с hemera

    await fastify.register(require('fastify-hemera'), {
        hemera: {
            name: 'gateway',
            logLevel: 'error',
            childLogger: false,
            tag: 'hemera-gateway'
        },
        nats: {
            url: config.nats.url,
        }
    });

    // await fastify.register(groupRoutes);
    await fastify.register(require('./modules/history'));
    await fastify.register(require('./modules/images.js'));

    fastify.listen(config.gateway.port, "0.0.0.0", (err, address) => {
        if (err) throw err;
        fastify.log.info(`server listening on ${address}`);
    });
};

module.exports = start;