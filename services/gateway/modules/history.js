module.exports = (fastify, opts, next) => {

    fastify.route({
        method: 'POST',
        url: '/api/v1/history/add',
        handler: async (req, reply) => {
            const response = await req.hemera.act({
                topic: 'add',
                cmd: 'database',
                doctor_ip: req.body.doctor_ip,
                doctor_id: req.body.doctor_id,
                patient_id: req.body.patient_id,
                pressure: req.body.pressure,
                quality: req.body.quality,
                diagnosis: req.body.diagnosis,
                therapy: req.body.therapy,
                recommendations: req.body.recommendations
            });

            reply.send(
                response.data
            );
        }
    });

    fastify.route({
        method: 'DELETE',
        url: '/api/v1/history/delete',
        handler: async (req, reply) => {
            const response = await req.hemera.act({
                topic: 'delete',
                cmd: 'database',
                session_id: req.body.session_id
            });

            reply.send(
                response.data
            );
        }
    });

    fastify.route({
        method: 'POST',
        url: '/api/v1/history/get',
        handler: async (req, reply) => {
            const response = await req.hemera.act({
                topic: 'get',
                cmd: 'database',
                doctor_id: req.body.doctor_id,
            });

            reply.send(
                response.data
            );
        }
    });

    fastify.route({
        method: 'POST',
        url: '/api/v1/history/patient',
        handler: async (req, reply) => {
            const response = await req.hemera.act({
                topic: 'patient_history',
                cmd: 'database',
                patient_id: req.body.patient_id,
                page: req.body.page
            });

            reply.send(
                response.data
            );
        }
    });

    fastify.route({
        method: 'POST',
        url: '/api/v1/history/edit',
        handler: async (req, reply) => {
            const response = await req.hemera.act({
                topic: 'edit',
                cmd: 'database',
                doctor_ip: req.body.doctor_ip,
                doctor_id: req.body.doctor_id,
                patient_id: req.body.patient_id,
                session_id: req.body.session_id,
                pressure: req.body.pressure,
                quality: req.body.quality,
                diagnosis: req.body.diagnosis,
                therapy: req.body.therapy,
                recommendations: req.body.recommendations
            });

            reply.send(
                response.data
            );
        }
    });
    next();
};