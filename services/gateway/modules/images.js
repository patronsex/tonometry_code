const {frontend: {neural_network}} = require('../../config/production');
const axios = require('axios');
const {withAuth} = require('../helpers/auth');
const {createClient} = require("webdav");
const {webdav: {login, password, host}} = require('../../config/production');
const uuid = require('uuid');

const client = createClient(
    host,
    {
        username: login,
        password: password,
        digest: true
    }
);

module.exports = (fastify, opts, next) => {
    fastify.route({
        method: 'POST',
        url: '/api/v1/circles',
        handler: (req, reply) => {
            withAuth(req, reply, async (user) => {
                try {
                    const image = req.body.image;
                    const uuid_val = uuid.v4();
                    const date_val = Date().replace(/ /g, '-');
                    const filename = date_val + "-unmarked-" + uuid_val;
                    const buffer_unmarked = Buffer.from(image, 'base64');

                    const dir = user ? "/" + user.email + "/" : "/unauthorized/";

                    if (await client.exists(dir) === false) await client.createDirectory(dir);
                    await client.putFileContents(`${dir}${filename}.jpg`, buffer_unmarked, {overwrite: false}).catch(e => {
                        console.log(e);
                    });

                    if (image) {
                        await axios.post(`${neural_network}/circles`, {image})
                            .then(async res => {
                                const buffer_marked = Buffer.from(res.data.image, 'base64');
                                const filename = date_val + "-marked-" + uuid_val;

                                await client.putFileContents(`${dir}${filename}.jpg`, buffer_marked, {overwrite: false}).catch(e => {
                                    console.log(e);
                                });

                                reply.send({
                                    success: 1,
                                    data: res.data
                                });

                            })
                            .catch((e) => {
                                console.log(e);
                                reply.send({
                                    success: 0,
                                    message: "response to neural network failed"
                                });
                            });

                    } else {
                        reply.code(400);
                        reply.send({
                            success: 0,
                            message: 'image is required'
                        });
                    }
                } catch (e) {
                    console.log(e);
                }
            }, false);
        }
    });
    next();
};
