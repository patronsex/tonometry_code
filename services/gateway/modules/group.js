module.exports = (fastify, opts, next) => {
    fastify.route({
        method: 'POST',
        url: '/api/v1/group/create',
        handler: async (req, reply) => {
            const response = await req.hemera.act({
                topic: 'group',
                cmd: 'create',
                group_name: req.body.group_name,
                doctor_id: req.body.doctor_id
            });

            reply.send(
                response.data
            );
        }
    });

    fastify.route({
        method: 'POST',
        url: '/api/v1/group/add_member',
        handler: async (req, reply) => {
            const response = await req.hemera.act({
                topic: 'group',
                cmd: 'add_member',
                group_id: req.body.group_id,
                invite_doctor_id: req.body.doctor_id //ID приглашенного юзера
            });

            reply.send(
                response.data
            );
        }
    });

    fastify.route({
        method: 'POST',
        url: '/api/v1/group/delete_member',
        handler: async (req, reply) => {
            const response = await req.hemera.act({
                topic: 'group',
                cmd: 'delete_member',
                group_id: req.body.group_id,
                delete_doctor_id: req.body.doctor_id //ID удаляемого юзера
            });

            reply.send(
                response.data
            );
        }
    });
    next();
};