const axios = require('axios');
const {auth: {auth_server}} = require('../../config/production.json');
const https = require('https');

const httpsAgent = new https.Agent({rejectUnauthorized: false});

module.exports = {
    withAuth: async (request, reply, callback, required = true) => {
            const token = request.headers.authorization;
            const splited_token = token && token.split(' ');
            if (token && splited_token[1]) {
                await axios.post(`${auth_server}/oauth2/userinfo`, {}, {
                    httpsAgent: httpsAgent,
                    headers: {
                        Authorization: request.headers.authorization
                    }
                }).then(res => {
                    console.log(res.data);
                    if (typeof callback === "function") callback(res.data);
                }).catch(err => {
                    if (required || err.response.data.error_reason === "access_token_expired") {
                        reply.code(401);
                        reply.send(err.response.data);
                    } else {
                        callback(null);
                    }
                });
            } else {
                reply.code(401);
                reply.send({
                    success: 0,
                    message: 'token and client_id required'
                })
            }
    }
};