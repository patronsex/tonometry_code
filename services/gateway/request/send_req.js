const fs = require('fs');
const axios = require('axios');
const FormData = require('form-data');
const {frontend: {neural_network}} = require('../../config/production.json');

(async () => {
    const file = fs.readFileSync(`${__dirname}/nn-test2.jpg`);
    let data = new FormData();
    data.append('image', file);
    await axios.post(`${neural_network}/circles`, data).then(res => {
        console.log('Успешно: ', res.data);
    }).catch(err => {
        console.log('Ошибка: ', err.response.data);
    })
})();