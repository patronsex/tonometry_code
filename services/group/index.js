const mysql = require('promise-mysql');
const config = require('../config/production');
const Hemera = require('nats-hemera');
const HemeraJoi = require('hemera-joi');
const nats = require('nats').connect({ url: process.env.NATS_URL || config.nats.url});
console.log(process.env.NATS_URL || config.nats.url);

const hemera = new Hemera(nats, {
    logLevel: 'info',
    childLogger: true,
    tag: 'hemera-insert'
});

async function start() {
    const con = await mysql.createConnection({
        host: config.database.host,
        port: config.database.port,
        user: config.database.user,
        password: config.database.password,
        database: config.database.database
    });
    hemera.use(HemeraJoi);
    await hemera.ready();
    const Joi = hemera.joi;

    hemera.add({
            topic: 'group',
            cmd: 'create',
            group_name: Joi.string().required(),
            doctor_id: Joi.number().required()
        },
        async (req) => {
            try {
                const result = await con.query("INSERT INTO `groups` (group_name) VALUES (?)",[req.group_name]).catch(() => null);
                if(result !== null) {
                        const add_member = await con.query("INSERT INTO `groups_members` (doctor_id, group_id, access) VALUES (?,?,'1')",[req.doctor_id, result.insertId]).catch(() => null);
                            if(add_member !== null) {
                                return {success:1, message:"group_success"};
                            }
                            else {
                                //Удаляем группу, чтоб не засорять БД, в случае, если не удалось добавить создавшего группу пользователя в саму группу.
                                //Группа без владельца - мусор в бд.
                                await con.query("DELETE FROM `groups` WHERE ((`group_id` = ?))",[result.insertId]);
                                return {success:0, message:"group_add_member_error"};
                            }
                        }
                else{
                    return {success:0, message:"group_create_error"};
                }
            }
            catch (e) {
                return {success:0, message:"group_create_error_sql"};
            }
        });

    hemera.add({
            topic: 'group',
            cmd: 'add_member',
            group_id: Joi.number().required(),
            invite_doctor_id: Joi.number().required()
        },
        async (req) => {
            try {
                const add_member = await con.query("INSERT INTO `groups_members` (doctor_id, group_id, access) VALUES (?,?,'0')",[req.invite_doctor_id, req.group_id]).catch(() => null);
                if(add_member !== null) {
                    return {success:1, message:"group_add_member_success"};
                }
                else {
                    return {success:0, message:"group_add_member_error"};
                }
            }
            catch (e) {
                return {success:0, message:"group_add_member_error_sql"};
            }
        });

    hemera.add({
            topic: 'group',
            cmd: 'delete_member',
            group_id: Joi.number().required(),
            delete_doctor_id: Joi.number().required()
        },
        async (req) => {
            try {
                const delete_member = await con.query("DELETE FROM `groups_members` WHERE ((`group_id` = ? AND `doctor_id` = ?))",[req.group_id, req.delete_doctor_id]).catch(() => null);
                if(delete_member.affectedRows !== 0) { // add_member.affectedRows - возвращает количество удаленных записей. Если=0, значит было нечего удалять=>возвращаем ошибку удаления.
                    return {success:1, message:"group_delete_member_success"};
                }
                else {
                    return {success:0, message:"group_delete_member_error"};
                }
            }
            catch (e) {
                return {success:0, message:"group_delete_member_error_sql"};
            }
        });

}

module.exports = start;