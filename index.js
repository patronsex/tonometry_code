const groupService = require('./services/group');
const historyService = require('./services/history');
const gateway = require('./services/gateway');

(async () => {
    try {
        await historyService();
        console.log('history service was started');
    } catch (e) {
        console.log('history service start failed', e);
    }
    try {
        await groupService();
        console.log('group service was started');
    } catch (e) {
        console.log('group service was not started', e);
    }
    try {
        await gateway();
        console.log('gateway was started');
    } catch (e) {
        console.log('gateway was not started', e);
    }
})();