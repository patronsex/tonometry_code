# Tonometry API

API для проекта "Тонометрия"

## Список POST-методов

* `circles` (`host:port/circles`) - возвращает все данные после авторазметки для картинки
* `boxes` (`host:port/boxes`) – возвращает координаты тонометрических кругов на картинке
* `pressures` (`host:port/pressures`) – возвращает данные о тонометрических кругах для левого и правого глаза
* `ratios` (`host:port/ratios`) - возвращает отношения внутренних и внешних диаметров тонометрических кругов на картинке

### `/circles`

Входные параметры:
* `image` – файл картинки

Выходные параметры:
* json-структура с `name == "data"`, имеющая поля:
  * `success` – `True`, если запрос обработан корректно, или `False` – в противном случае
  * `boxes` – массив из массивов `(x1, y1, x2, y2)` – координат прямоугольников, в которые вписаны тонометрические круги. Возвращается, если `success == True`
  * `ratios` – массив из отношений внутренних и внешних диаметров тонометрических кругов, соответствует координатам из массива `boxes`. Возвращается, если `success == True`.
  * `eyes` - массив названий глаз, соответствующих координатам из массива `boxes`. Возможные значение: `0` (левый глаз) или `1` (правый глаз) 
  * `masses` - массив масс гирек, соответствующих координатам из массива `boxes`. Возможные значение: `5`, `10` или `15`
  * `mask` - массив из дробей. K-ый элемент равен 1, если в K-ый прямоугольник в массиве `boxes` вписан корректный круг. Если круг имеет деффект, то K-ый элемент стремится к 0
* изображение с `name == "file"`, `filename == "image.jpg"` и `content-type == "image/jpeg"`

Возвращается `multipart/form-data` 

Пример запроса на Python:

```python
import requests
from requests_toolbelt import MultipartDecoder

with open("file.jpg", "rb") as f:
    image = f.read()

payload = {"image": image}

circles_request = requests.post("http://localhost:5000/boxes", files=payload).json()
decoder = MultipartDecoder.from_response(circles_request)

json_data = None
image_data = None
for part in decoder.parts:
    header_value = part.headers[b'Content-Disposition'].decode("utf-8").lower()
    if 'name="data"' in header_value:
        json_data = part.text
    elif 'name="file"' in header_value:
        image_data = part.content

if json_data:
    json_data = json.loads(json_data)
    print(json_data)
    if json_data["success"]:
        with open("tmp.jpg", "wb+") as f:
            f.write(image_data)
    else:
        print("fail")
else:
    print("fail")
```

### `/boxes`

Входные параметры:
* `image` – файл картинки

Выходные параметры:
* json-структура, имеющая поля:
  * `success` – `True`, если запрос обработан корректно, или `False` – в противном случае
  * `boxes` – массив из массивов `(x1, y1, x2, y2)` – координат прямоугольников, в которые вписаны тонометрические круги.
  Возвращается, если `success == True`
  * `mask` - массив из дробей. K-ый элемент равен 1, если в K-ый прямоугольник в массиве `boxes` вписан корректный круг. Если круг имеет деффект, то K-ый элемент равен стремится к 0

Пример запроса на Python:

```python
import requests

with open("file.jpg", "rb") as f:
    image = f.read()

payload = {"image": image}

boxes_response = requests.post("http://localhost:5000/boxes", files=payload).json()
if boxes_response["success"]:
    print("boxes:", boxes_response["boxes"])
    print("mask", boxes_response["mask"])
else:
    print("fail")
```

### `/pressures`

Входные параметры:
* `image` – файл картинки
* `json` - json-структура с описанием тонометрических кругов. В запросе передается как файл и имеет поля:
  * `boxes` - массив из массивов `(x1, y1, x2, y2)` – координат прямоугольников, в которые вписаны тонометрические круги
  * `masses` - массив масс гирек, соответствующих координатам из массива `boxes`. Возможные значение: `5`, `10` или `15`
  * `eyes` - массив названий глаз, соответствующих координатам из массива `boxes`. Возможные значение: 
`0` (левый глаз) или `1` (правый глаз)

Выходные параметры:
* `success` – `True`, если запрос обработан корректно, или `False` – в противном случае
* `pressures` – json-структура, возвращается, если `success == True`. Имеет поля:
  * `left` - содержит массив [`p5`, `p10`, `p15`, `p0`, `ep`], соответствующий параметрам левого глаза:
    - `p5` - давление при гирьке в 5 грамм
    - `p10` - давление при гирьке в 10 грамм
    - `p15` - давление при гирьке в 15 грамм
    - `p0` - внутриглазное давление
    - `ep` - эластопластичность глаза
  * `right` - содержит массив [`p5`, `p10`, `p15`, `p0`, `ep`], соответствующий параметрам правого глаза:
    - `p5` - давление при гирьке в 5 грамм
    - `p10` - давление при гирьке в 10 грамм
    - `p15` - давление при гирьке в 15 грамм
    - `p0` - внутриглазное давление
    - `ep` - эластопластичность глаза

Если в массиве `eyes` были значения только для одного глаза (например, в массиве находились **только** значения `0` или **только** значения `1`), то для другого глаза в массиве `pressures` вернется пустой список

Пример запроса на Python:

```python
import json
import requests

with open("file.jpg", "rb") as f:
    image = f.read()

boxes = [[2069, 1844, 2432, 2203], [1189, 1870, 1552, 2239], [2868, 1255, 3241, 1635]]
masses = [5, 10, 15]
eyes = [0, 1, 0]

json_data = {"boxes": boxes, "masses": masses, "eyes": eyes}
payload = {"image": image, "json": json.dumps(json_data)}

pressures_response = requests.post("http://localhost:5000/pressures", files=payload).json()
if pressures_response["success"]:
    pres = pressures_response["pressures"]

    left_p5, left_p10, left_p15, left_p0, left_ep = pres["left"]
    right_p5, right_p10, right_p15, right_p0, right_ep = pres["right"]
```

### `/ratios`

Входные параметры:
* `image` – файл картинки
* `json` - json-структура с описанием тонометрических кругов. В запросе передается как файл и имеет поля:
  * `boxes` - массив из массивов `(x1, y1, x2, y2)` – координат прямоугольников, в которые вписаны тонометрические круги

Выходные параметры:
* `success` – `True`, если запрос обработан корректно, или `False` – в противном случае
* `ratios` – массив из отношений внутренних и внешних диаметров тонометрических кругов, соответствует координатам из массива `boxes`. Возвращается, если `success == True`.

Пример запроса на Python:

```python
import json
import requests

with open("file.jpg", "rb") as f:
    image = f.read()

boxes = [[2069, 1844, 2432, 2203], [1189, 1870, 1552, 2239], [2868, 1255, 3241, 1635]]

json_data = {"boxes": boxes}
payload = {"image": image, "json": json.dumps(json_data)}

ratios_response = requests.post("http://localhost:5000/ratios", files=payload).json()
if ratios_response["success"]:
    ratios = ratios_response["ratios"]

    for box, ratio in zip(boxes, ratios):
        print(box, ratio)
```

### `/recommender_system_maklakov`

Входные параметры:
* `json` - json-структура с описанием параметров пациенты. В запросе передается как файл и имеет поля:
  * `Pt`
  * `Stage`
  * `PG`
  * `AM`
  * `IKA`
  * `BB`
  * `Retinaprotection`
  * `Sum`

Выходные параметры:
* `success` – `True`, если запрос обработан корректно, или `False` – в противном случае
* `recommendation` – json-структура, возвращается, если `success == True`, и имеет поля:
  * `time`
  * `rec_drug`
  * `diagnostic`
  * `add_drug`
  * `rp`

Пример запроса на Python:

```python
import json
import requests

json_data = {"Pt": 15, "Stage": 0, "PG": 0, "AM": 0, "IKA": 0, "BB": 0, "Retinaprotection": 0, "Sum": 0}
payload = {"json": json.dumps(json_data)}

recommendation_response = requests.post("http://localhost:5000/recommender_system_maklakov", files=payload).json()
if recommendation_response["success"]:
    recommendation = recommendation_response["recommendation"]
    print(recommendation)
```

### `/recommender_system_elasto`

Входные параметры:
* `json` - json-структура с описанием параметров пациенты. В запросе передается как файл и имеет поля:
  * `P0`
  * `Stage`
  * `PG`
  * `AM`
  * `IKA`
  * `BB`
  * `Retinaprotection`
  * `eye`

Выходные параметры:
* `success` – `True`, если запрос обработан корректно, или `False` – в противном случае
* `recommendation` – json-структура, возвращается, если `success == True`, и имеет поля:
  * `eye`
  * `time`
  * `rec_drug`
  * `diagnostic`
  * `add_drug`
  * `rp`

Пример запроса на Python:

```python
import json
import requests

json_data = {"P0": 15, "Stage": 0, "PG": 0, "AM": 0, "IKA": 0, "BB": 0, "Retinaprotection": 0, "eye": 0}
payload = {"json": json.dumps(json_data)}

recommendation_response = requests.post("http://localhost:5000/recommender_system_elasto", files=payload).json()
if recommendation_response["success"]:
    recommendation = recommendation_response["recommendation"]
    print(recommendation)
```
